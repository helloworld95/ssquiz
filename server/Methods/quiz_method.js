import {
	GameSessions
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
import {
	Answers
} from '../Collections/Collections';
import {
	Quizs
} from '../Collections/Collections';


var getSlug = require('speakingurl');

Meteor.methods({
	'quiz.create': function(quiz) {
		try {
			//if user is not logged in then throw an exception
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			//check if the param "quiz" is an Object, otherwise throwing an error
			check(quiz, Object);
			//get a friendly URL for the quiz based on it's title
			var friendlyURL = getSlug(quiz.title, {
				lang: 'en'
			});
			//check if there are any duplicated URLs, throwing an error when there are some 
			var duplicateURLs = Quizs.find({
				url: friendlyURL
			}).fetch();
			if (duplicateURLs.length > 0)
				throw new Meteor.Error("Duplicated-URL");
			//insert the new quiz
			var entityID = Quizs.insert({
				title: quiz.title,
				description: quiz.description,
				editedBy: this.userId,
				createdAt: new Date().getTime(),
				editedAt: new Date().getTime(),
				status: 'draft',
				owner: this.userId,
				url: friendlyURL
			});
			return friendlyURL;
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'quiz.update.title': function(quiz, title) {
		try {
			//if user is not logged in then throw an exception
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			//check if the param "quiz" an "title" are Strings, otherwise throwing an error
			check(quiz, String);
			check(title, String);
			//get a friendly URL for the quiz based on it's new title
			var friendlyURL = getSlug(title, {
				lang: 'en'
			});
			//check if there are any duplicated URLs, throwing an error when there are some 
			var duplicateURLs = Quizs.find({
				url: friendlyURL
			}).fetch();
			if (duplicateURLs.length > 0)
				throw new Meteor.Error("Duplicated-URL");
			//check if the quiz is published, if so, prevent update
			if (Quizs.findOne({
					owner: this.userId,
					_id: quiz,
					status: "published"
				})) throw new Meteor.Error("Cannot-update-because-it-is-in-used");
			//update the title
			Quizs.update({
				owner: this.userId,
				_id: quiz,
				status: {
					$ne: "published"
				}
			}, {
				$set: {
					title: title,
					url: friendlyURL,
					editedAt: new Date().getTime()
				}
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'quiz.update.description': function(quiz, description) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(quiz, String);
			check(description, String);
			//check if the quiz is published, if so, prevent update
			if (Quizs.findOne({
					owner: this.userId,
					_id: quiz,
					status: "published"
				})) throw new Meteor.Error("Cannot-update-because-it-is-in-used");
			Quizs.update({
				owner: this.userId,
				_id: quiz,
				status: {
					$ne: "published"
				}
			}, {
				$set: {
					description: description,
					editedAt: new Date().getTime()
				}
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'quiz.requestPublish': function(quizURL) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(quizURL, String);
			if (Quizs.findOne({
					owner: this.userId,
					url: quizURL,
					$or: [{
						status: "pending"
					}, {
						status: "published"
					}]
				})) throw new Meteor.Error("Already-sent-request");
			if (Roles.userIsInRole(this.userId, ['admin'], 'calmandlearn.com'))
				Quizs.update({
					owner: this.userId,
					url: quizURL,
					status: 'draft'
				}, {
					$set: {
						status: 'published'
					}
				});;
			Quizs.update({
				owner: this.userId,
				url: quizURL,
				$or: [{
					status: 'draft'
				}, {
					status: 'denied'
				}]
			}, {
				$set: {
					status: 'pending'
				}
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'quiz.remove': function(quiz) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(quiz, String);
			//check if the quiz is published, if so, prevent update
			if (Quizs.findOne({
					owner: this.userId,
					_id: quiz,
					status: "published"
				})) throw new Meteor.Error("Cannot-update-because-it-is-in-used");
			Quizs.update({
				owner: this.userId,
				_id: quiz,
				status: {
					$ne: "published"
				}
			}, {
				$set: {
					status: 'remove',
					editedAt: new Date().getTime()
				}
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}
});