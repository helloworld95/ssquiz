import {
	GameSessions
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
import {
	Answers
} from '../Collections/Collections';
import {
	Quizs
} from '../Collections/Collections';

var getSlug = require('speakingurl');

Meteor.methods({
	'question.create': function(quizURL, question) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(question, String);
			check(quizURL, String);
			let quizID = Quizs.findOne({
				owner: this.userId,
				url: quizURL,
				status: {
					$ne: "published"
				}
			}, {
				fields: {
					_id: 1
				}
			});
			if (!quizID._id) {
				throw new Meteor.Error("Cannot-update-because-it-is-in-used-or-not-exist");
			}

			let position = Questions.find({
				quiz: quizID._id
			}).count() + 1;
			return Questions.insert({
				question: question,
				position: position,
				createdAt: new Date().getTime(),
				editedBy: this.userId,
				editedAt: new Date().getTime(),
				owner: this.userId,
				quiz: quizID._id
			});
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'question.update': function(questionID, question) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(questionID, String);
			check(question, String);
			//get the question document
			let tempQuestion = Questions.findOne({
				owner: this.userId,
				_id: questionID
			});
			if (!tempQuestion)
				throw new Meteor.Error("Not-authorized");

			//check if the quiz is published, if so, prevent update
			let tempQuiz = Quizs.findOne({
				owner: this.userId,
				_id: tempQuestion.quiz,
				status: "published"
			})
			if (tempQuiz)
				throw new Meteor.Error("Cannot-update-because-it-is-in-used");
			Questions.update({
				owner: this.userId,
				_id: questionID
			}, {
				$set: {
					question: question,
					editedAt: new Date().getTime(),
				}
			});
			return true;
		} catch (e) {
			console.log(e)
			return false;
		}
	},
	'question.remove': function(questionID) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(questionID, String);
			//get the question document
			let tempQuestion = Questions.findOne({
				owner: this.userId,
				_id: questionID
			});
			if (!tempQuestion)
				throw new Meteor.Error("Not-authorized");

			//check if the quiz is published, if so, prevent update
			let tempQuiz = Quizs.findOne({
				owner: this.userId,
				_id: tempQuestion.quiz,
				status: "published"
			})
			if (tempQuiz)
				throw new Meteor.Error("Cannot-update-because-it-is-in-used");
			Questions.remove({
				owner: this.userId,
				_id: questionID
			});
			Answers.remove({
				owner: this.userId,
				question: questionID
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}
});