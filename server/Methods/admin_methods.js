import {
	GameSessions
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
import {
	Answers
} from '../Collections/Collections';
import {
	Quizs
} from '../Collections/Collections';


var getSlug = require('speakingurl');



Meteor.methods({
	'admin.publishQuiz': function(quizID) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			if (!Roles.userIsInRole(this.userId, ['admin'], 'calmandlearn.com'))
				throw new Meteor.Error("Not-authorized-admin");
			check(quizID, String);
			Quizs.update({
				_id: quizID,
				status: "pending"
			}, {
				$set: {
					status: 'published'
				}
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'admin.denyQuiz': function(quizID) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			if (!Roles.userIsInRole(this.userId, ['admin'], 'calmandlearn.com'))
				throw new Meteor.Error("Not-authorized-admin");
			check(quizID, String);
			Quizs.update({
				_id: quizID,
				status: "pending"
			}, {
				$set: {
					status: 'denied'
				}
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'admin.takedownQuiz': function(quizID) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			checkAdmin();
			check(quizID, String);
			Quizs.update({
				_id: quizID,
				status: "published"
			}, {
				$set: {
					status: 'draft'
				}
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'admin.changeUserPassword': function(userID, newPassword) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			checkAdmin();
			check(userID, String);
			Accounts.setPassword(userID, newPassword);
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}
})