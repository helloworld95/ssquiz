import {
	GameSessions
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
import {
	Answers
} from '../Collections/Collections';
import {
	Quizs
} from '../Collections/Collections';
var getSlug = require('speakingurl');

Meteor.methods({
	'answer.create': function(questionID, answer, isRight) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(questionID, String);
			check(answer, String);
			check(isRight, Boolean);
			//get the Question document
			let tempQuestion = Questions.findOne({
				owner: this.userId,
				_id: questionID
			});
			if (!tempQuestion) {
				throw new Meteor.Error("Not-authorized");
			}
			//check if the quiz is published, if so, prevent creating
			let tempQuiz = Quizs.findOne({
				owner: this.userId,
				_id: tempQuestion.quiz,
				status: "published"
			})
			if (tempQuiz)
				throw new Meteor.Error("Cannot-update-because-it-is-in-used");

			let position = Answers.find({
				question: questionID
			}).count() + 1;
			return Answers.insert({
				answer: answer,
				position: position,
				createdAt: new Date().getTime(),
				editedBy: this.userId,
				editedAt: new Date().getTime(),
				owner: this.userId,
				question: questionID,
				isRight: isRight
			});
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'answer.update': function(answerID, answer) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(answerID, String);
			check(answer, String);
			//get the Answer document 
			let tempAnswer = Answers.findOne({
				_id: answerID,
				owner: this.userId
			});
			if (!tempAnswer)
				throw new Meteor.Error("Not-found");
			//get the Question document
			let tempQuestion = Questions.findOne({
				_id: tempAnswer.question,
				owner: this.userId
			});
			if (!tempQuestion)
				throw new Meteor.Error("Not-found");
			//check if the quiz is published, if so, prevent creating
			let tempQuiz = Quizs.findOne({
				owner: this.userId,
				_id: tempQuestion.quiz,
				status: "published"
			})
			if (tempQuiz)
				throw new Meteor.Error("Cannot-update-because-it-is-in-used");
			Answers.update({
				owner: this.userId,
				_id: answerID
			}, {
				$set: {
					answer: answer,
					editedAt: new Date().getTime()
				}
			});
			return true;
		} catch (e) {
			console.log(e)
			return false;
		}
	},
	'answer.update.isRight': function(answerID, isRight) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(answerID, String);
			check(isRight, Boolean);
			//get the Answer document 
			let tempAnswer = Answers.findOne({
				_id: answerID,
				owner: this.userId
			});
			if (!tempAnswer)
				throw new Meteor.Error("Not-found");
			//get the Question document
			let tempQuestion = Questions.findOne({
				_id: tempAnswer.question,
				owner: this.userId
			});
			if (!tempQuestion)
				throw new Meteor.Error("Not-found");
			//check if the quiz is published, if so, prevent creating
			let tempQuiz = Quizs.findOne({
				owner: this.userId,
				_id: tempQuestion.quiz,
				status: "published"
			})
			if (tempQuiz)
				throw new Meteor.Error("Cannot-update-because-it-is-in-used");
			Answers.update({
				owner: this.userId,
				_id: answerID
			}, {
				$set: {
					isRight: isRight,
					editedAt: new Date().getTime()
				}
			});
			return true;
		} catch (e) {
			console.log(e)
			return false;
		}
	},
	'answer.remove': function(answerID) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(answerID, String);
			//get the Answer document 
			let tempAnswer = Answers.findOne({
				_id: answerID,
				owner: this.userId
			});
			if (!tempAnswer)
				throw new Meteor.Error("Not-found");
			//get the Question document
			let tempQuestion = Questions.findOne({
				_id: tempAnswer.question,
				owner: this.userId
			});
			if (!tempQuestion)
				throw new Meteor.Error("Not-found");
			//check if the quiz is published, if so, prevent creating
			let tempQuiz = Quizs.findOne({
				owner: this.userId,
				_id: tempQuestion.quiz,
				status: "published"
			})
			if (tempQuiz)
				throw new Meteor.Error("Cannot-update-because-it-is-in-used");
			Answers.remove({
				owner: this.userId,
				_id: answerID
			});
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}
});