import {
	GameSessions
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
import {
	Answers
} from '../Collections/Collections';
import {
	Quizs
} from '../Collections/Collections';


var getSlug = require('speakingurl');

// const checkvalid = (record, isArray, collection) =>{
// 	if (collection=="Quizs") {
// 		if (isArray) 


// 	} else if (collection=="Answers") {

// 	} else if (collection=="Questions") {

// 	} else if (collection=="GameSessions") {

// 	} 
// 	return false;
// }

Meteor.methods({
	'GameSessions.create': function(quizURL) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(quizURL, String);
			let quizID = Quizs.findOne({
				url: quizURL,
				status: "published"
			}, {
				fields: {
					_id: 1
				}

			});
			if (!quizID._id) {
				throw new Meteor.Error("Not-authorized");
			}

			let QuestionCursor = Questions.find({
				quiz: quizID._id
			}, {
				fields: {
					_id: 1,
				}
			});
			//Number of questions
			let TotalScore = QuestionCursor.count();
			//get all questions' IDs
			let questionArr = QuestionCursor.fetch().map((question) => {
				let questionID = question._id;
				//get all right answers
				let rightAnswers = Answers.find({
					question: questionID,
					isRight: true
				}, {
					fields: {
						_id: 1
					}
				}).fetch().map((answer) => {
					return answer._id;
				});
				return {
					question: questionID,
					rightAnswers: rightAnswers
				}
			});

			if (!TotalScore || TotalScore <= 0) throw new Meteor.Error("No-Questions");
			return GameSessions.insert({
				quizID: quizID._id,
				status: "started",
				startedAt: new Date().getTime(),
				finishedAt: new Date().getTime(),
				player: this.userId,
				questions: questionArr,
				usersAnswers: [],
				finalScore: 0,
				totalScore: TotalScore
			});
		} catch (e) {
			console.log(e);
			return false;
		}
	},
	'GameSessions.finish': function(GameSessionsID, answerIDs) {
		try {
			if (!this.userId) {
				throw new Meteor.Error("Not-authorized");
			}
			check(GameSessionsID, String);
			check(answerIDs, [String]);
			let finishedTime = new Date().getTime();
			//check user's answers
			let answerCheck = Answers.find({
				_id: {
					$in: answerIDs
				}
			}).fetch();
			if (!answerCheck) throw new Meteor.Error("Not-found-Answer");
			//Get the Game session and the QuizID
			let GameSession = GameSessions.findOne({
				_id: GameSessionsID,
				status: "started"
			}, {
				fields: {
					quizID: 1,
					questions: 1
				}

			});
			if (!GameSession) throw new Meteor.Error("Not-found-Game");

			//calculate the user's score 
			// 1. get all questions that the user answered
			let usersAnswerDocs = Answers.find({
				_id: {
					$in: answerIDs
				}
			}, {
				fields: {
					_id: 1,
					question: 1
				}

			}).fetch();
			let usersAnsweredQues = usersAnswerDocs.map((answer) => {
				return answer.question;
			});
			//2. Produces a duplicate-free version of the user's answered questions
			usersAnsweredQues = _.uniq(usersAnsweredQues);
			//3. loop through all answered questions and add user's answers to them.
			let userQuesAndAns = usersAnsweredQues.map((questionID) => {
				let answersofQues = [];
				usersAnswerDocs.map((answer) => {
					if (answer.question == questionID)
						answersofQues.push(answer._id);
				});
				return {
					question: questionID,
					useranswers: answersofQues
				};
			});
			//count score
			let userscore = 0;
			GameSession.questions.map((question) => {
				userQuesAndAns.map((userQues) => {
					if(userQues.question == question.question) {
						if (_.isEqual(userQues.useranswers.sort(), question.rightAnswers.sort())) 
							userscore++;
					}
				})
			})

			//insertScore and finished time
			GameSessions.update({
				player: this.userId,
				_id: GameSessionsID,
				status: "started"
			}, {
				$set: {
					status: "finished",
					finishedAt: finishedTime,
					finalScore: userscore
				},
				$addToSet: {
					usersAnswers: {
						$each: answerIDs
					}
				}
			});
			return GameSessionsID;
		} catch (e) {
			console.log(e)
			return false;
		}
	}
});