Accounts.emailTemplates.siteName = "AwesomeSite";
Accounts.emailTemplates.from = "AwesomeSite Admin <accounts@example.com>";
Accounts.emailTemplates.enrollAccount.subject = function (user) {
    return "Welcome to Awesome Town, " + user.profile.name;
};
Accounts.emailTemplates.enrollAccount.text = function (user, url) {
   return "You have been selected to participate in building a better future!"
     + " To activate your account, simply click the link below:\n\n"
     + url;
};
Accounts.emailTemplates.resetPassword.from = function () {
   // Overrides value set in Accounts.emailTemplates.from when resetting passwords
   return "AwesomeSite Password Reset <no-reply@example.com>";
};
Meteor.methods({
	'account.create': function(fullname, username, email, password) {
		try {
			check(fullname, String);
			check(username, String);
			check(email, String);
			check(password, String);
			if (fullname=='') 
				throw new Meteor.Error("fullname-notfound");
			if (username=='') 
				throw new Meteor.Error("username-notfound");
			if (email=='') 
				throw new Meteor.Error("email-notfound");
			if (password=='') 
				throw new Meteor.Error("password-notfound");
			
			var userID = Accounts.createUser({
				username: username,
				email: email,
				password: password,
				profile: {
					name: fullname
				}
			});
			Roles.addUsersToRoles(userID, ['users', 'admin'], 'calmandlearn.com');
			return true;
		} catch (e) {
			console.log(e);
			return false;
		}
	}
});