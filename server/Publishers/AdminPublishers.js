import {
	Answers
} from '../Collections/Collections'
import {
	Quizs
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
import {
	Users
} from '../Collections/Collections';

Meteor.publish("listAllQuizs", function() {
	try {
		if (!Roles.userIsInRole(this.userId, ['admin'], 'calmandlearn.com'))
			throw new Meteor.Error("Not-authorized-admin");
		return Quizs.find({
			$or: [{
				status: "pending"
			}, {
				status: "published"
			}, {
				status: "cancelpending"
			}]
		}, {
			fields: {
				_id: 1,
				title: 1,
				description: 1,
				url: 1,
				createdAt: 1,
				status: 1
			}
		});
	} catch (e) {
		console.log(e);
	}
});


Meteor.publish("quizDetailViewer", function(quizURL) {
	try {
		if (!Roles.userIsInRole(this.userId, ['admin'], 'calmandlearn.com'))
			throw new Meteor.Error("Not-authorized-admin");
		let thequiz = Quizs.findOne({
			url: quizURL
		}, {
			fields: {
				_id: 1,
				title: 1,
				description: 1,
				url: 1,
				createdAt: 1,
				status: 1
			}
		});
		let quizCursor = Quizs.find({
			_id: thequiz._id
		}, {
			fields: {
				url: 1,
				status: 1
			}
		});
		let questioncursor = Questions.find({
			quiz: thequiz._id
		}, {
			fields: {
				_id: 1,
				question: 1
			}
		});

		let questionIDs = questioncursor.fetch().map((question) => {
			return question._id;
		});

		let answercursor = Answers.find({
			question: {
				$in: questionIDs
			}
		}, {
			fields: {
				_id: 1,
				answer: 1,
				question: 1,
				isRight: 1
			}

		});

		return [quizCursor, questioncursor, answercursor];
	} catch (e) {
		console.log(e);
	}
});



Meteor.publish("listUsers", function() {
	try {
		if (!Roles.userIsInRole(this.userId, ['admin'], 'calmandlearn.com'))
			throw new Meteor.Error("Not-authorized-admin");
		return Meteor.users.find({}, {
			fields: {
				_id: 1,
				emails: 1,
				profile: 1,
				roles: 1,
				createdAt: 1
			}
		});
	} catch (e) {
		console.log(e);
	}
});