import {
	Answers
} from '../Collections/Collections'
import {
	Quizs
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
import {
	GameSessions
} from '../Collections/Collections';

Meteor.publish("listHistory", function() {
	try {
		let gameCursor = GameSessions.find({
			player: this.userId
		}, {
			fields: {
				_id: 1,
				quizID: 1,
				startedAt: 1,
				finishedAt: 1,
				finalScore: 1,
				totalScore: 1
			}
		});
		let quizIDs = gameCursor.fetch().map((game) => {
			return game.quizID;
		});
		let quizCursor = Quizs.find({
			_id: {
				$in: quizIDs
			}
		}, {
			fields: {
				_id: 1,
				title: 1
			}

		})
		return [gameCursor,quizCursor];
	} catch (e) {
		console.log(e);
	}
});

Meteor.publish("historyDetail", function(gameID) {
	try {
		check(gameID, String);
		let gameCursor = GameSessions.find({
			player: this.userId,
			_id: gameID
		}, {
			fields: {
				_id: 1,
				quizID: 1,
				startedAt: 1,
				finishedAt: 1,
				finalScore: 1,
				totalScore: 1
			}
		});
		let quizIDs = gameCursor.fetch().map((game) => {
			return game.quizID;
		});
		let quizCursor = Quizs.find({
			_id: {
				$in: quizIDs
			}
		}, {
			fields: {
				_id: 1,
				title: 1
			}

		})
		return [gameCursor,quizCursor];
	} catch (e) {
		console.log(e);
	}
});