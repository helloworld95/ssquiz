import {
	Answers
} from '../Collections/Collections'
import {
	Quizs
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
import {
	Users
} from '../Collections/Collections';

Meteor.publish("listPublishQuizs", function() {
	let quizCursor = Quizs.find({
		status: 'published'
	}, {
		fields: {
			title: 1,
			description: 1,
			url: 1,
			owner: 1
		}
	});
	// let ownersIDs = quizCursor.fetch().map((quiz) => {
	// 	return quiz.owner;
	// })
	// let ownerCursor = Users.find({
	// 	_id: {
	// 		$in: ownersIDs
	// 	}
	// }, {
	// 	fields: {
	// 		_id: 1,
	// 		"profile.name": 1
	// 	}
	// });
	return [quizCursor];
});

Meteor.publish("listQuestionsOfAQuiz", function(quizURL) {
	check(quizURL, String);
	let quiz = Quizs.findOne({
		url: quizURL,
		status: 'published'
	}, {
		fields: {
			_id: 1,
		}

	});
	let quizCursor = Quizs.find({
		url: quizURL,
		status: 'published'
	}, {
		fields: {
			title: 1
		}

	});
	if (!quiz._id) {
		throw new Meteor.Error("Not-found");
	}
	let QuesCursor = Questions.find({
		quiz: quiz._id
	}, {
		fields: {
			_id: 1,
			question: 1
		}
	});
	let QuestionsArr = QuesCursor.fetch();
	let QuesIds = QuestionsArr.map((question) => {
		return question._id;
	})
	let AnswerCursor = Answers.find({
		question: {
			$in: QuesIds
		}
	}, {
		fields: {
			_id: 1,
			question: 1,
			answer: 1,
			position: 1,
			isRight: 1
		}

	});
	return [QuesCursor, AnswerCursor, quizCursor];
});


// Meteor.publish("displayAnswersOnDoingAQuestion", function(questionID) {
// 	check(questionID, String);
// 	let questionIDcheck = Question.findOne({
// 		_id: questionID
// 	}, {
// 		fields: {
// 			_id: 1
// 		}

// 	});
// 	if (!questionIDcheck._id) {
// 		throw new Meteor.Error("Not-found");
// 	}
// 	return Answer.find({
// 		question: questionID
// 	}, {
// 		fields: {
// 			answer: 1,
// 			_id: 1
// 		}
// 	});
// });