import {
	Answers
} from '../Collections/Collections'
import {
	Quizs
} from '../Collections/Collections';
import {
	Questions
} from '../Collections/Collections';
Meteor.publish("listOwnQuizs", function() {
	return Quizs.find({
		owner: this.userId,
	}, {
		fields: {
			_id: 1,
			title: 1,
			description: 1,
			createdAt: 1,
			status: 1,
			url: 1
		}
	});
});
Meteor.publish("listQuesBasedOnQuiz", function(quizURL) {
	check(quizURL, String);
	let quizID = Quizs.findOne({
		url: quizURL,
	}, {
		fields: {
			_id: 1
		}
	});

	let QuestionCursor = Questions.find({
		owner: this.userId,
		quiz: quizID._id
	}, {
		fields: {
			_id: 1,
			question: 1,
			createdAt: 1
		}
	});
	let QuestionIDs = QuestionCursor.fetch().map((question) => {
		return question._id;
	});
	let AnswerCursor = Answers.find({
		owner: this.userId,
		question: {
			$in: QuestionIDs
		}
	}, {
		fields: {
			_id: 1,
			answer: 1,
			question: 1,
			createdAt: 1,
			isRight: 1
		}

	});
	return [QuestionCursor, AnswerCursor];
});
// Meteor.publish("listAnswers", function() {
// 	return Answers.find({
// 		owner: this.userId,
// 	}, {
// 		fields: {
// 			_id: 1,
// 			answer: 1,
// 			question: 1,
// 			createdAt: 1,
// 			isRight: 1
// 		}
// 	});
// });