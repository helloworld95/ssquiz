import ActionTypes from './ActionTypes';

const doUpdateQuestion = (questionID, question) => ({
	type: ActionTypes.UPDATE_QUESTION,
	questionID: questionID,
	question: question
})

const successUpdateQuestion = () => ({
	type: ActionTypes.UPDATE_QUESTION_SUCCESS
})

const failedUpdateQuestion = () => ({
	type: ActionTypes.UPDATE_QUESTION_FAILED
})


export default {
	doUpdateQuestion,
	successUpdateQuestion,
	failedUpdateQuestion
}