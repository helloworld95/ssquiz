import ActionTypes from './ActionTypes';

const doCreateQuiz = (quiz) => ({
	type: ActionTypes.CREATE_QUIZ,
	quiz: quiz
})

const successCreateQuiz = (quizURL) => ({
	type: ActionTypes.CREATE_QUIZ_SUCCESS,
	quizURL: quizURL
})

const failedCreateQuiz = () => ({
	type: ActionTypes.CREATE_QUIZ_FAILED
})


export default {
	doCreateQuiz,
	successCreateQuiz,
	failedCreateQuiz
}