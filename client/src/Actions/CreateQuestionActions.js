import ActionTypes from './ActionTypes';

const doCreateQuestion = (quizURL, question) => ({
	type: ActionTypes.CREATE_QUESTION,
	quizURL: quizURL,
	question: question
})

const successCreateQuestion = () => ({
	type: ActionTypes.CREATE_QUESTION_SUCCESS
})

const failedCreateQuestion = () => ({
	type: ActionTypes.CREATE_QUESTION_FAILED
})


export default {
	doCreateQuestion,
	successCreateQuestion,
	failedCreateQuestion
}