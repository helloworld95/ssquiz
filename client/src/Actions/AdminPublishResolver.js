import ActionTypes from './ActionTypes';

const doAcceptRequestPublish = (quizID) => ({
	type: ActionTypes.ACCEPT_REQUEST_PUBLISH,
	quizID: quizID
})

const successAcceptRequestPublish = () => ({
	type: ActionTypes.ACCEPT_REQUEST_PUBLISH_SUCCESS
})

const failedAcceptRequestPublish = () => ({
	type: ActionTypes.ACCEPT_REQUEST_PUBLISH_FAILED
})

const doDenyRequestPublish = (quizID) => ({
	type: ActionTypes.DENY_REQUEST_PUBLISH,
	quizID: quizID
})

const successDenyRequestPublish = () => ({
	type: ActionTypes.DENY_REQUEST_PUBLISH_SUCCESS
})

const failedDenyRequestPublish = () => ({
	type: ActionTypes.DENY_REQUEST_PUBLISH_FAILED
})


export default {
	doAcceptRequestPublish,
	successAcceptRequestPublish,
	failedAcceptRequestPublish,
	doDenyRequestPublish,
	successDenyRequestPublish,
	failedDenyRequestPublish
}