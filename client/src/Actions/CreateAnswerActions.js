import ActionTypes from './ActionTypes';

const doCreateAnswer = (questionID, answer, isRight) => ({
	type: ActionTypes.CREATE_ANSWER,
	questionID: questionID,
	answer: answer,
	isRight: isRight
})

const successCreateAnswer = () => ({
	type: ActionTypes.CREATE_ANSWER_SUCCESS
})

const failedCreateAnswer = () => ({
	type: ActionTypes.CREATE_ANSWER_FAILED
})


export default {
	doCreateAnswer,
	successCreateAnswer,
	failedCreateAnswer
}