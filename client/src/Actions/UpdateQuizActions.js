import ActionTypes from './ActionTypes';

const doUpdateTitleQuiz = (quiz, title) => ({
	type: ActionTypes.UPDATE_TITLE_QUIZ,
	quiz: quiz,
	title: title
})

const successUpdateTitleQuiz = () => ({
	type: ActionTypes.UPDATE_TITLE_QUIZ_SUCCESS
})

const failedUpdateTitleQuiz = () => ({
	type: ActionTypes.UPDATE_TITLE_QUIZ_FAILED
})

const doUpdateDesQuiz = (quiz, description) => ({
	type: ActionTypes.UPDATE_DESCRIPTION_QUIZ,
	quiz: quiz,
	description: description
})

const successUpdateDesQuiz = () => ({
	type: ActionTypes.UPDATE_DESCRIPTION_QUIZ_SUCCESS
})

const failedUpdateDesQuiz = () => ({
	type: ActionTypes.UPDATE_DESCRIPTION_QUIZ_FAILED
})

const requestPublishQuiz = (quizURL) => ({
	type: ActionTypes.REQUEST_PUBLISH_QUIZ,
	quizURL: quizURL
})

const successRequestPublishQuiz = () => ({
	type: ActionTypes.REQUEST_PUBLISH_QUIZ_SUCCESS
})

const failedRequestPublishQuiz = () => ({
	type: ActionTypes.REQUEST_PUBLISH_QUIZ_FAILED
})

export default {
	doUpdateTitleQuiz,
	successUpdateTitleQuiz,
	failedUpdateTitleQuiz,
	doUpdateDesQuiz,
	successUpdateDesQuiz,
	failedUpdateDesQuiz,
	requestPublishQuiz,
	successRequestPublishQuiz,
	failedRequestPublishQuiz
}