import ActionTypes from './ActionTypes';

const doCreateGame = (quizURL) => ({
	type: ActionTypes.CREATE_GAME_SESSION,
	quizURL: quizURL
})

const successCreateGame = (GameSessionsID, quizURL) => ({
	type: ActionTypes.CREATE_GAME_SESSION_SUCCESS,
	GameSessionsID: GameSessionsID,
	quizURL: quizURL
})

const failedCreateGame = () => ({
	type: ActionTypes.CREATE_GAME_SESSION_FAILED
})


export default {
	doCreateGame,
	successCreateGame,
	failedCreateGame
}