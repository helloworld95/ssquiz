import ActionTypes from './ActionTypes';

const doUpdateAnswer = (answerID, answer) => ({
	type: ActionTypes.UPDATE_ANSWER,
	answerID: answerID,
	answer: answer
})

const successUpdateAnswer = () => ({
	type: ActionTypes.UPDATE_ANSWER_SUCCESS
})

const failedUpdateAnswer = () => ({
	type: ActionTypes.UPDATE_ANSWER_FAILED
})

const doUpdateAnswerRight = (answerID, isRight) => ({
	type: ActionTypes.UPDATE_ANSWER_RIGHT,
	answerID: answerID,
	isRight: isRight
})

const successUpdateAnswerRight = () => ({
	type: ActionTypes.UPDATE_ANSWER_RIGHT_SUCCESS
})

const failedUpdateAnswerRight = () => ({
	type: ActionTypes.UPDATE_ANSWER_RIGHT_FAILED
})

export default {
	doUpdateAnswer,
	successUpdateAnswer,
	failedUpdateAnswer,
	doUpdateAnswerRight,
	successUpdateAnswerRight,
	failedUpdateAnswerRight
}