import ActionTypes from './ActionTypes';

const doRegister = (fullname, username, email, password) => ({
	type: ActionTypes.REGISTER,
	fullname: fullname,
	username: username,
	email: email,
	password: password
})

const successRegister = () => ({
	type: ActionTypes.REGISTER_SUCCESS
})

const failedRegister = () => ({
	type: ActionTypes.REGISTER_FAILED
})

const redirectOnSuccesRegister = () => ({
	type: ActionTypes.REGISTER_SUCCESS_REDIRECT
})


export default {
	doRegister,
	successRegister,
	failedRegister,
	redirectOnSuccesRegister
}