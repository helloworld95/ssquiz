import ActionTypes from './ActionTypes';

const doRemoveQuiz = (quiz) => ({
	type: ActionTypes.REMOVE_QUIZ,
	quiz: quiz
})

const successRemoveQuiz = () => ({
	type: ActionTypes.REMOVE_QUIZ_SUCCESS
})

const failedRemoveQuiz = () => ({
	type: ActionTypes.REMOVE_QUIZ_FAILED
})


export default {
	doRemoveQuiz,
	successRemoveQuiz,
	failedRemoveQuiz
}