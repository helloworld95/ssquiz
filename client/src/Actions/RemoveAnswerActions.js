import ActionTypes from './ActionTypes';

const doRemoveAnswer = (answerID) => ({
	type: ActionTypes.REMOVE_ANSWER,
	answerID: answerID
})

const successRemoveAnswer = () => ({
	type: ActionTypes.REMOVE_ANSWER_SUCCESS
})

const failedRemoveAnswer = () => ({
	type: ActionTypes.REMOVE_ANSWER_FAILED
})


export default {
	doRemoveAnswer,
	successRemoveAnswer,
	failedRemoveAnswer
}