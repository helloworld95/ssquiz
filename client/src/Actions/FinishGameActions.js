import ActionTypes from './ActionTypes';

const doFinishGame = (GameSessionsID, answerIDs) => ({
	type: ActionTypes.FINISH_QUIZ,
	GameSessionsID: GameSessionsID,
	answerIDs: answerIDs
})

const successFinishGame = (gameID) => ({
	type: ActionTypes.FINISH_QUIZ_SUCCESS,
	gameID: gameID
})

const failedFinishGame = () => ({
	type: ActionTypes.FINISH_QUIZ_FAILED
})


export default {
	doFinishGame,
	successFinishGame,
	failedFinishGame
}