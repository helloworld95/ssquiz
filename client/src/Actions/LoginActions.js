import ActionTypes from './ActionTypes';

const doLogin = (username, password) => ({
	type: ActionTypes.LOGIN,
	username: username,
	password: password
})

const successLogin = () => ({
	type: ActionTypes.LOGIN_SUCCESS
})

const failedLogin = () => ({
	type: ActionTypes.LOGIN_FAILED
})


export default {
	doLogin,
	successLogin,
	failedLogin
}