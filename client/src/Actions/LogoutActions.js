import ActionTypes from './ActionTypes';

const doLogout = () => ({
	type: ActionTypes.LOGOUT
})

const doneLogout = () => ({
	type: ActionTypes.DONE_LOGOUT
})
export default {
	doLogout,
	doneLogout
}