import ActionTypes from './ActionTypes';

const doRemoveQuestion = (questionID) => ({
	type: ActionTypes.REMOVE_QUESTION,
	questionID: questionID
})

const successRemoveQuestion = () => ({
	type: ActionTypes.REMOVE_QUESTION_SUCCESS
})

const failedRemoveQuestion = () => ({
	type: ActionTypes.REMOVE_QUESTION_FAILED
})


export default {
	doRemoveQuestion,
	successRemoveQuestion,
	failedRemoveQuestion
}