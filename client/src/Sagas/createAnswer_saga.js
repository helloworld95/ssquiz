import {
	hashHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Answer from '../API/API_Answer';
import CreateAnswerActions from '../Actions/CreateAnswerActions';
import {updateQuizSuccessNotification, updateQuizFailedNotification} from '../Notification_config/ManageQuiz_noti';


function* createAnswer(action) {
	try {
		console.log(action)
		const chan = yield call(API_Answer.createNewAnswer, action.questionID, action.answer, action.isRight);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(CreateAnswerActions.failedCreateAnswer());
			yield put(createNotification(updateQuizFailedNotification));
		} else {
			yield put(CreateAnswerActions.successCreateAnswer());
			yield put(createNotification(updateQuizSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}


export default {
	createAnswer
}