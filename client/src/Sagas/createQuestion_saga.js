import {
	hashHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Question from '../API/API_Question';
import CreateQuestionActions from '../Actions/CreateQuestionActions';
import {updateQuizSuccessNotification, updateQuizFailedNotification} from '../Notification_config/ManageQuiz_noti';


function* createQuestion(action) {
	try {
		const chan = yield call(API_Question.createNewQuestion, action.quizURL, action.question);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(CreateQuestionActions.failedCreateQuestion());
			yield put(createNotification(updateQuizFailedNotification));
		} else {
			yield put(CreateQuestionActions.successCreateQuestion());
			yield put(createNotification(updateQuizSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}


export default {
	createQuestion
}