import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import ActionTypes from '../Actions/ActionTypes';
import LoginSaga from './login_saga';
import LogoutSaga from './logout_saga';
import RegisterSaga from './register_saga';
import CreateQuizSaga from'./createQuiz_saga';
import UpdateQuizSaga from './updateQuiz_saga';
import RemoveQuizSaga from './removeQuiz_saga';
import CreateQuestionSaga from'./createQuestion_saga';
import UpdateQuestionSaga from'./updateQuestion_saga';
import RemoveQuestionSaga from'./removeQuestion_saga';
import CreateAnswerSaga from'./createAnswer_saga';
import UpdateAnswerSaga from'./updateAnswer_saga';
import RemoveAnswerSaga from'./removeAnswer_saga';
import CreateGameSaga from'./createGame_saga';
import FinishGameSaga from'./finishGame_saga';
import RequestPublishQuizSaga from'./requestPublish_saga';
import ResolvePublishRequestSaga from './resolvePublishRequest_saga';


export default function*() {
	yield [
		takeLatest(ActionTypes.REGISTER, RegisterSaga.doRegisterUser),
		takeLatest(ActionTypes.REGISTER_SUCCESS, RegisterSaga.redirectUserToLogin),
		takeLatest(ActionTypes.LOGIN, LoginSaga.doLoginUser),
		takeLatest(ActionTypes.LOGIN_SUCCESS, LoginSaga.redirectUserToHomepage),
		takeLatest(ActionTypes.LOGOUT, LogoutSaga.doLogoutUser),
		takeLatest(ActionTypes.DONE_LOGOUT, RegisterSaga.redirectUserToLogin),
		takeEvery(ActionTypes.CREATE_QUIZ, CreateQuizSaga.createQuiz),
		takeEvery(ActionTypes.CREATE_QUIZ_SUCCESS, CreateQuizSaga.redirectToQuizEditor),
		takeEvery(ActionTypes.UPDATE_TITLE_QUIZ, UpdateQuizSaga.updateTitleQuiz),
		takeEvery(ActionTypes.UPDATE_DESCRIPTION_QUIZ, UpdateQuizSaga.updateDescriptonQuiz),
		takeEvery(ActionTypes.REMOVE_QUIZ, RemoveQuizSaga.removeQuiz),
		takeEvery(ActionTypes.CREATE_QUESTION, CreateQuestionSaga.createQuestion),
		takeEvery(ActionTypes.UPDATE_QUESTION, UpdateQuestionSaga.updateQuestion),
		takeEvery(ActionTypes.REMOVE_QUESTION, RemoveQuestionSaga.removeQuestion),
		takeEvery(ActionTypes.CREATE_ANSWER, CreateAnswerSaga.createAnswer),
		takeEvery(ActionTypes.UPDATE_ANSWER, UpdateAnswerSaga.updateAnswer),
		takeEvery(ActionTypes.UPDATE_ANSWER_RIGHT, UpdateAnswerSaga.updateAnswerRight),
		takeEvery(ActionTypes.REMOVE_ANSWER, RemoveAnswerSaga.removeAnswer),
		takeLatest(ActionTypes.CREATE_GAME_SESSION, CreateGameSaga.createGameSession),
		takeLatest(ActionTypes.FINISH_QUIZ, FinishGameSaga.finishGameSession),
		takeLatest(ActionTypes.FINISH_QUIZ_SUCCESS, FinishGameSaga.displayResult),
		takeLatest(ActionTypes.CREATE_GAME_SESSION_SUCCESS, CreateGameSaga.redirectToGame),
		takeLatest(ActionTypes.REQUEST_PUBLISH_QUIZ, RequestPublishQuizSaga.requestPublishQuiz),
		takeLatest(ActionTypes.ACCEPT_REQUEST_PUBLISH, ResolvePublishRequestSaga.acceptPublishRequest),
		takeLatest(ActionTypes.DENY_REQUEST_PUBLISH, ResolvePublishRequestSaga.denyPublishRequest)
	];
}
