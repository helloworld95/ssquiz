import {
	browserHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API from '../API/API';
import LoginActions from '../Actions/LoginActions';
import {loginSuccessNotification, loginFailedNotification} from '../Notification_config/Account_noti';

function* doLoginUser(action) {
	try {
		const chan = yield call(API.loginUser, action.username, action.password);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(LoginActions.failedLogin());
			yield put(createNotification(loginFailedNotification));
		} else {
			yield put(LoginActions.successLogin());
			yield put(createNotification(loginSuccessNotification));
		}
	} finally {
		console.log('terminated')
	}
}

function* redirectUserToHomepage(action) {
	browserHistory.push({
		pathname: '/'
	});
}

export default {
	doLoginUser, 
	redirectUserToHomepage
}