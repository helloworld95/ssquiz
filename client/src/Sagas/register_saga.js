import {
	browserHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API from '../API/API';
import RegisterActions from '../Actions/RegisterActions';
import {registerSuccessNotification, registerFailedNotification} from '../Notification_config/Account_noti';

function* doRegisterUser(action) {
	try {
		const chan = yield call(API.registerUser, action.fullname, action.username, action.email, action.password);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(RegisterActions.failedRegister());
			yield put(createNotification(registerFailedNotification));
		} else {
			yield put(RegisterActions.successRegister());
			yield put(createNotification(registerSuccessNotification));
		}
	} finally {
		console.log('terminated')
	}
}

function* redirectUserToLogin(action) {
	browserHistory.push({
		pathname: '/page/login'
	});
}

export default {
	doRegisterUser,
	redirectUserToLogin
}