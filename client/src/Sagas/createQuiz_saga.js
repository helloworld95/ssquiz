import {
	browserHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Quiz from '../API/API_Quiz';
import CreateQuizActions from '../Actions/CreateQuizActions';
import {addQuizSuccessNotification, addQuizFailedNotification} from '../Notification_config/ManageQuiz_noti';


function* createQuiz(action) {
	try {
		const chan = yield call(API_Quiz.createNewQuiz, action.quiz);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(CreateQuizActions.failedCreateQuiz());
			yield put(createNotification(addQuizFailedNotification));
		} else {
			yield put(CreateQuizActions.successCreateQuiz(data));
			yield put(createNotification(addQuizSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}

function* redirectToQuizEditor (action) {
	browserHistory.push({
		pathname: '/myquizs/'+action.quizURL
	});
}

export default {
	createQuiz,
	redirectToQuizEditor
}