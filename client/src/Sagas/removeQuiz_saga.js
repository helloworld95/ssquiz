import {
	hashHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Quiz from '../API/API_Quiz';
import RemoveQuizActions from '../Actions/RemoveQuizActions';
import {removeQuizSuccessNotification, removeQuizFailedNotification} from '../Notification_config/ManageQuiz_noti';


function* removeQuiz(action) {
	try {
		const chan = yield call(API_Quiz.removeQuiz, action.quiz);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(RemoveQuizActions.failedRemoveQuiz());
			yield put(createNotification(removeQuizFailedNotification));
		} else {
			yield put(RemoveQuizActions.successRemoveQuiz());
			yield put(createNotification(removeQuizSuccessNotification));
		}
	} finally {
		console.log('terminated')
	}
}

export default {
	removeQuiz
}