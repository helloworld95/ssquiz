import {
	hashHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Quiz from '../API/API_Quiz';
import UpdateQuizActions from '../Actions/UpdateQuizActions';
import {updateQuizSuccessNotification, updateQuizFailedNotification} from '../Notification_config/ManageQuiz_noti';


function* requestPublishQuiz(action) {
	try {
		const chan = yield call(API_Quiz.requestPublishQuiz, action.quizURL);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(UpdateQuizActions.failedRequestPublishQuiz());
			yield put(createNotification(updateQuizFailedNotification));
		} else {
			yield put(UpdateQuizActions.successRequestPublishQuiz());
			yield put(createNotification(updateQuizSuccessNotification));
		}
	} finally {
		console.log('terminated')
	}
}

export default {
	requestPublishQuiz
}