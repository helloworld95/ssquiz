import {
	browserHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import ActionTypes from '../Actions/ActionTypes';
import API from '../API/API';
import LogoutActions from '../Actions/LogoutActions';
import {
	createNotification
} from 'react-redux-notify';
import {logoutNotification} from '../Notification_config/Account_noti';


function* doLogoutUser(action) {
	try {
		const chan = yield call(API.logoutUser);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		yield put(LogoutActions.doneLogout());
		yield put(createNotification(logoutNotification));
	} finally {
		console.log('terminated')
	}
}

// function* doLogoutUser(action) {
// 	try {
// 		const chan = yield call(API.logoutUser);
// 	} catch (e) {
// 		console.log(e.message);
// 	}

// }


export default {
	doLogoutUser
}