import {
	hashHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Quiz from '../API/API_Quiz';
import UpdateQuizActions from '../Actions/UpdateQuizActions';
import {updateQuizSuccessNotification, updateQuizFailedNotification} from '../Notification_config/ManageQuiz_noti';


function* updateTitleQuiz(action) {
	try {
		const chan = yield call(API_Quiz.updateTitleQuiz, action.quiz, action.title);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(UpdateQuizActions.failedUpdateTitleQuiz());
			yield put(createNotification(updateQuizFailedNotification));
		} else {
			yield put(UpdateQuizActions.successUpdateTitleQuiz());
			yield put(createNotification(updateQuizSuccessNotification));
		}
	} finally {
		console.log('terminated')
	}
}

function* updateDescriptonQuiz(action) {
	try {
		const chan = yield call(API_Quiz.updateDescriptionQuiz, action.quiz, action.description);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(UpdateQuizActions.failedUpdateDesQuiz());
			yield put(createNotification(updateQuizFailedNotification));
		} else {
			yield put(UpdateQuizActions.successUpdateDesQuiz());
			yield put(createNotification(updateQuizSuccessNotification));
		}
	} finally {
		console.log('terminated')
	}
}
export default {
	updateTitleQuiz,
	updateDescriptonQuiz
}