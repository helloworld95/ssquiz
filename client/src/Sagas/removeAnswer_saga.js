import {
	hashHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Answer from '../API/API_Answer';
import RemoveAnswerActions from '../Actions/RemoveAnswerActions';
import {updateQuizSuccessNotification, updateQuizFailedNotification} from '../Notification_config/ManageQuiz_noti';


function* removeAnswer(action) {
	try {
		const chan = yield call(API_Answer.removeAnswer, action.answerID);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(RemoveAnswerActions.failedRemoveAnswer());
			yield put(createNotification(updateQuizFailedNotification));
		} else {
			yield put(RemoveAnswerActions.successRemoveAnswer());
			yield put(createNotification(updateQuizSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}


export default {
	removeAnswer
}