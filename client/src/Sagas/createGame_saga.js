import {
	browserHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_doQuiz from '../API/API_doQuiz';
import CreateGameActions from '../Actions/CreateGameActions';
import {playGameSuccessNotification, playGameFailedNotification} from '../Notification_config/Game_noti';


function* createGameSession(action) {
	try {
		const chan = yield call(API_doQuiz.createGameSession, action.quizURL);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(CreateGameActions.failedCreateGame());
			yield put(createNotification(playGameFailedNotification));
		} else {
			yield put(CreateGameActions.successCreateGame(data,action.quizURL));
			yield put(createNotification(playGameSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}

function* redirectToGame (action) {
	browserHistory.push({
		pathname: '/publishedQuizs/'+action.quizURL,
		state: { sessionID: action.GameSessionsID }
	});
}

export default {
	createGameSession,
	redirectToGame
}