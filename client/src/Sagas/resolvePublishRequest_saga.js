import {
	hashHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Admin from '../API/API_Admin';
import AdminPublishResolver from '../Actions/AdminPublishResolver';
import {acceptSuccessNotification, acceptFailedNotification, denySuccessNotification, denyFailedNotification} from '../Notification_config/Admin_noti';


function* acceptPublishRequest(action) {
	try {
		const chan = yield call(API_Admin.acceptPublishRequest, action.quizID);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(AdminPublishResolver.failedAcceptRequestPublish());
			yield put(createNotification(acceptFailedNotification));
		} else {
			yield put(AdminPublishResolver.successAcceptRequestPublish());
			yield put(createNotification(acceptSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}

function* denyPublishRequest(action) {
	try {
		const chan = yield call(API_Admin.denyPublishRequest, action.quizID);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(AdminPublishResolver.failedDenyRequestPublish());
			yield put(createNotification(denyFailedNotification));
		} else {
			yield put(AdminPublishResolver.successDenyRequestPublish());
			yield put(createNotification(denySuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}

export default {
	acceptPublishRequest,
	denyPublishRequest
}