import {
	hashHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_Answer from '../API/API_Answer';
import UpdateAnswerActions from '../Actions/UpdateAnswerActions';
import {updateQuizSuccessNotification, updateQuizFailedNotification} from '../Notification_config/ManageQuiz_noti';


function* updateAnswer(action) {
	try {
		const chan = yield call(API_Answer.updateAnswer, action.answerID, action.answer);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(UpdateAnswerActions.failedUpdateAnswer());
			yield put(createNotification(updateQuizFailedNotification));
		} else {
			yield put(UpdateAnswerActions.successUpdateAnswer());
			yield put(createNotification(updateQuizSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}

function* updateAnswerRight(action) {
	try {
		const chan = yield call(API_Answer.updateAnswerisRight, action.answerID, action.isRight);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(UpdateAnswerActions.failedUpdateAnswerRight());
			yield put(createNotification(updateQuizFailedNotification));
		} else {
			yield put(UpdateAnswerActions.successUpdateAnswerRight());
			yield put(createNotification(updateQuizSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}


export default {
	updateAnswer,
	updateAnswerRight
}