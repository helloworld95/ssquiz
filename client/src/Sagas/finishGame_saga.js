import {
	browserHistory
} from 'react-router';
import {
	call,
	put,
	takeEvery,
	takeLatest,
	take
} from 'redux-saga/effects';
import {
	createNotification
} from 'react-redux-notify';
import ActionTypes from '../Actions/ActionTypes';
import API_doQuiz from '../API/API_doQuiz';
import FinishGameActions from '../Actions/FinishGameActions';
import {finishQuizSuccessNotification, finishQuizFailedNotification} from '../Notification_config/Game_noti';


function* finishGameSession(action) {
	try {
		console.log(action)
		const chan = yield call(API_doQuiz.finishQuiz, action.GameSessionsID, action.answerIDs);
		// take(END) will cause the saga to terminate by jumping to the finally block
		let data = yield take(chan);
		if (data == false) {
			yield put(FinishGameActions.failedFinishGame());
			yield put(createNotification(finishQuizFailedNotification));
		} else {
			yield put(FinishGameActions.successFinishGame(data));
			yield put(createNotification(finishQuizSuccessNotification));
			
		}
	} finally {
		console.log('terminated')
	}
}

function* displayResult(action) {
	browserHistory.push({
		pathname: '/history/'+action.gameID
	});
}
export default {
	finishGameSession,
	displayResult
}