import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import { Provider } from 'react-redux';
import store from './Store/store';

class App extends React.Component {
	render() {
		return (
			<Provider store={store}><Router routes={routes} history={browserHistory}/></Provider>
		);
	}
}


if (Meteor.isClient) {
	Meteor.startup(() => {
		ReactDOM.render(
  			<App />, document.getElementById('root')
		);
	})
}

