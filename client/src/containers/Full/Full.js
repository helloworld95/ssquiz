import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Footer from '../../components/Footer/';
import {Notify} from 'react-redux-notify';
import Breadcrumbs from 'react-breadcrumbs';

class Full extends Component {
  render() {
    return (
      
      <div className="app">
      {this.props.ready ? 
        <div>
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumbs
              wrapperElement="ol"
              wrapperClass="breadcrumb"
              itemClass="breadcrumb-item"
              separator=""
              routes={this.props.routes}
              params={this.props.params}
            />
            <div className="container-fluid">
              <Notify />
              {this.props.children}
            </div>
          </main>
        </div>
        <Footer /></div> : <div className="initial-loading"></div>}
      </div>
    );
  }
}

//export default Full;
export default createContainer((data) => {
  var handle = Roles.subscription;
  if (handle.ready())
  return {
    ready: true
  };
  return {ready: false};
}, Full);