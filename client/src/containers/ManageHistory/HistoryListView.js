import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import dateFormat from 'dateformat';
import {GameSessions} from '../../Collections/Collections';
import {Quizs} from '../../Collections/Collections';
import timeformat from 'format-duration';
import { Link } from 'react-router'

class HistoryListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      currentHisSelected: '',
      currentQuizSelected: '',
      date: ''
    }
    this.renderRows=this.renderRows.bind(this);
    this.openDetailModal=this.openDetailModal.bind(this);
    this.closeDetailModal=this.closeDetailModal.bind(this);
  }
  componentWillUnmount() {
    try {
      this.props.handler.stop();
    } catch (e) {
      console.log('the handler has not started yet! It is ok!');
    }
  }
  
  openDetailModal(game, quiz, date) {
    this.setState({
      currentHisSelected: game,
      currentQuizSelected: quiz,
      date: date
    }, () => {
      this.setState({
        modal: true
      })
    })
  }
  closeDetailModal() {
    this.setState({
        modal: false
    })
  }
  renderRows() {
    let rows = [];
    if (!this.props.games) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!</td></tr>;
    if (this.props.games.lenght<=0) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!</td></tr>;
    this.props.games.map((game, index) => {
      let hisQuiz = this.props.quizs.findOne({_id: game.quizID});
      let StartedDate = dateFormat(new Date(game.startedAt), "d/mm/yyyy HH:MM:ss");
      let time = timeformat(game.finishedAt - game.startedAt);
      rows.push(
        <tr key={index}>
          <td>{hisQuiz.title}
          </td>
          <td>{StartedDate}
          </td>
          <td>{game.finalScore + '/' + game.totalScore}</td>
          <td>
            {time + " giây"}
          </td>
          <td>
            <Link to={'/history/'+game._id} className="btn btn-outline-primary btn-sm" style={{cursor: "pointer"}}>Chi tiết </Link>
          </td>
        </tr>
      );
    });
    return rows;
  }
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Quiz</th>
              <th>Ngày tham gia</th>
              <th>Số điểm</th>
              <th>Thời gian hoàn thành</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
            {this.props.ready ? this.renderRows() : <tr><td><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</td></tr>}
          </tbody>
        </table>
      </div>
    );
  }
}

export default createContainer((data) => {
  var handle = Meteor.subscribe('listHistory');
  if (handle.ready())
  return {
    ready: true,
    handler: handle,
    games: GameSessions.find({}).fetch(),
    quizs: Quizs
  };
  return {ready: false};
}, HistoryListView);
