import React, { Component } from 'react';
import HistoryListView from './HistoryListView';

class ManageHistoryView extends Component {
  constructor(props) {
    super(props);
    this.state={
      modal: false,
      searchkey: ''
    };
    this.onKeywordChange=this.onKeywordChange.bind(this);
    this.toggleModal=this.toggleModal.bind(this);
  }
  toggleModal() {
    this.setState({
      modal: !this.state.modal
    })
  }
  onKeywordChange(event) {
    this.setState({
      searchkey: event.target.value
    })
  }
  render() {
    return (
      <div className="animated bounce">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header">
                <i className="fa fa-align-justify"></i> Lịch sử
              </div>
              <div className="card-block">
                <HistoryListView/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ManageHistoryView;