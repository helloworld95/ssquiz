import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {GameSessions, Quizs} from '../../Collections/Collections';
import dateFormat from 'dateformat';
import timeformat from 'format-duration';
import { Pie } from 'react-chartjs-2';

class HistoryDetailView extends Component {
  constructor(props) {
    super(props);
    this.renderDetail=this.renderDetail.bind(this);
  }
  componentWillUnmount() {
    try {
      this.props.handler.stop();
    } catch (e) {
      console.log('the handler has not started yet! It is ok!');
    }
  }
  renderDetail() {
    const dataset = {
      labels: [
        'Đúng',
        'Sai'
      ],
      datasets: [{
        data: [
          this.props.game.finalScore,
          this.props.game.totalScore - this.props.game.finalScore
        ],
        backgroundColor: [
          '#66e29c',
          '#f45555'
        ],
        hoverBackgroundColor: [
        '#05c458',
        '#fc0000'
        ]
      }]
    };
    let StartedDate = dateFormat(new Date(this.props.game.startedAt), "d/mm/yyyy HH:MM:ss");
    let time = timeformat(this.props.game.finishedAt - this.props.game.startedAt);
    let quiz = this.props.quizs.findOne({_id: this.props.game.quizID});
    return (
      <div>
        <div style={{float: "left", display: "inline-block"}}>
        <strong>Quiz:</strong><p>{quiz.title}</p>
         <strong>Ngày tham gia:</strong><p>{StartedDate}</p>
         <strong>Thời gian hoàn thành:</strong><p>{time + ' giây'}</p>
         <strong>Số điểm:</strong><p>{this.props.game.finalScore + '/' + this.props.game.totalScore}</p><br/>
         </div>
         <div className="chart-wrapper" style={{width: "50%",float: "right", display: "inline-block"}}>
            <Pie data={dataset} />
          </div>

      </div>);
  }
  render() {
    return (
      <div className="animated bounce">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header">
                <i className="fa fa-align-justify"></i> Chi tiết
              </div>
              <div className="card-block">
              {this.props.ready ? this.renderDetail() : <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div>}

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default createContainer((data) => {
  var handle = Meteor.subscribe('historyDetail', data.params.gameID);
  if (handle.ready())
  return {
    ready: true,
    handler: handle,
    game: GameSessions.findOne({_id: data.params.gameID}),
    quizs: Quizs
  };
  return {ready: false};
}, HistoryDetailView);