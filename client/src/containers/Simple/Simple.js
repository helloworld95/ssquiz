import React, { Component } from 'react';
import {Notify} from 'react-redux-notify';

class Simple extends Component {
  render() {
    return (
      <div className="app flex-row align-items-center">
      <Notify />
        {this.props.children}
      </div>
    );
  }
}

export default Simple;
