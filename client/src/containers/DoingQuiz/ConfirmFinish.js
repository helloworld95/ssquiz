import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import FinishGameActions from '../../Actions/FinishGameActions';
import {bindActionCreators} from 'redux';

class ModalFinishQuiz extends Component {
  constructor(props) {
    super(props);
    
    this.onConfirmHandler=this.onConfirmHandler.bind(this);
  }
  onConfirmHandler() {
    this.props.finishQuiz(this.props.GameSession,this.props.useranswers);
  }
  render() {
    return (
      <Modal isOpen={this.props.open} toggle={this.props.onToggle} className={'modal-lg ' + this.props.className}>
        <ModalHeader toggle={this.toggle}>Hoàn thành?</ModalHeader>
        <ModalBody>
        Hãy chắc chắn rằng bạn đã hoàn thành. Bạn đồng ý?
        </ModalBody>
        <ModalFooter>
        {this.props.data.loading ? <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div> :
          <div> 
            <Button color="danger" onClick={this.onConfirmHandler}>Đồng ý</Button>{' '}
            <Button color="secondary" onClick={this.props.onToggle}>Hủy</Button>
          </div>
        }
        </ModalFooter>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {data: state.finishquiz};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({finishQuiz: FinishGameActions.doFinishGame}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ModalFinishQuiz);