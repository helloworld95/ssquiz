import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Questions } from '../../Collections/Collections';
import WrapperCheckbox from './WrapperCheckbox';
import CountdownTimer from './CountdownTimer';
import removeValue from 'remove-value';
import _ from 'lodash';
import timeformat from 'format-duration';

class QuestionView extends Component {
	constructor(props){
		super(props);
		this.state = {
			question: this.props.question,
			position: this.props.position,
			answers: this.props.answers,
			rightAnswers: this.props.answers.filter(answer => answer.isRight).map(answer => answer._id),
			finalAnswers: [],
			isShuffling: false,
			showRightAnswers: false,
			triedTimes: 0,
			startedQuestionAt: new Date().getTime(),
			time: null,
		}
		this.checkedAnswerBeforeMove = this.checkedAnswerBeforeMove.bind(this);
		this.renderQuestion=this.renderQuestion.bind(this);
		this.renderAnswers=this.renderAnswers.bind(this);
		this.addRemoveUserAnswer=this.addRemoveUserAnswer.bind(this);
		this.addUserAnswer=this.addUserAnswer.bind(this);
    	this.removeUserAnswer=this.removeUserAnswer.bind(this);
		this.finishQuiz = this.finishQuiz.bind(this);
	}
	finishQuiz() {
		var time = timeformat((new Date().getTime()) - this.state.startedQuestionAt);
		this.props.addQuestionResult(this.state.question.question, this.props.answers.filter(answer => answer.isRight).map(answer => answer.answer), this.state.triedTimes, time, this.props.finishQuiz)
	}
	checkedAnswerBeforeMove() {
		console.log('result',_.isEqual(this.state.rightAnswers.sort(), this.state.finalAnswers.sort()))
		// this.props.changeNextQuestion
		if(_.isEqual(this.state.rightAnswers.sort(), this.state.finalAnswers.sort())) {
			this.setState({
				showRightAnswers:true,
			},() => {
				var time = timeformat((new Date().getTime()) - this.state.startedQuestionAt);
				this.props.addQuestionResult(this.state.question.question, this.props.answers.filter(answer => answer.isRight).map(answer => answer.answer), this.state.triedTimes, time)
				setTimeout(function(){ 
					this.props.changeNextQuestion()
				}.bind(this), 1000);
			})
		} else {
			var shuffleAnswers = _.shuffle(this.state.answers);
			this.setState({
				showRightAnswers:true,
				triedTimes: this.state.triedTimes+1,
			})
			setTimeout(function(){ 
				this.setState({
					answers: [],
					finalAnswers: [],
					isShuffling: true,
					showRightAnswers: false,
				})
			}.bind(this), 1000);
			setTimeout(function(){ 
				this.setState({
					answers: shuffleAnswers,
					isShuffling: false
				})
			}.bind(this), 5000);
		}
	}
	addRemoveUserAnswer(event, answerID) {
		if (event.target.checked) {
			this.addUserAnswer(answerID);
		} else {
			this.removeUserAnswer(answerID);
		}
	}
	addUserAnswer(answerID) {
		let ansTemp = this.state.finalAnswers;
		ansTemp.push(answerID);
		this.setState({
			finalAnswers: ansTemp
		}, () => {
			console.log("Q",this.state.finalAnswers)
		})
	}
	removeUserAnswer(answerID) {
		let ansTemp = removeValue(this.state.finalAnswers, answerID);
		this.setState({
			finalAnswers: ansTemp
		}, () => {
			console.log("Q",this.state.finalAnswers)
		})
	}
	renderAnswers() {
		let answers = this.state.answers;
		let ansTemp = this.state.finalAnswers;
		let rightAns = this.state.rightAnswers;
		let rows = answers.map((answer, index) => {
			if (this.state.showRightAnswers) {
				if(rightAns.includes(answer._id)) {
					return (
						<div key={index}>
							<WrapperCheckbox nameColor="#50f442" name={answer.answer} checked={ansTemp.includes(answer._id)} onChange={(event) => {this.addRemoveUserAnswer(event, answer._id)}}/>
						</div>
					);
				} else if(ansTemp.includes(answer._id) && !rightAns.includes(answer._id)) {
					return (
						<div key={index}>
							<WrapperCheckbox nameColor="red"name={answer.answer} checked={ansTemp.includes(answer._id)} onChange={(event) => {this.addRemoveUserAnswer(event, answer._id)}}/>
						</div>
					);
				} else {
					return (
						<div key={index}>
							<WrapperCheckbox nameColor="" name={answer.answer} checked={ansTemp.includes(answer._id)} onChange={(event) => {this.addRemoveUserAnswer(event, answer._id)}}/>
						</div>
					);
				}
			} else {
				return (
					<div key={index}>
						<WrapperCheckbox nameColor="" name={answer.answer} checked={ansTemp.includes(answer._id)} onChange={(event) => {this.addRemoveUserAnswer(event, answer._id)}}/>
					</div>
				);
			}
		});
		return rows;
	}
	renderQuestion(){
		let question = this.state.question;
		return (
		<div className="col-sm-6 col-md-4 animated bounceIn" style={{margin: "0 auto", float: "none", marginBottom: "10px"}}>
            <div className="card" style={{width:"100%"}}>
              <div className="card-header">
                <strong style={{display: "inline"}}>
                Câu hỏi số {this.state.position}:
                </strong><br/>
                &nbsp;
                {question.question}
              </div>
              <div className="card-block">
                <strong style={{display: "inline-block"}}>
                Các câu trả lời: &nbsp; 
                </strong>
				{this.state.isShuffling? <CountdownTimer secondsRemaining="4"/>:null}
                {this.renderAnswers()}
            </div>
          </div>
		  {this.props.isLast? <button disabled={this.state.finalAnswers.length>0? false : true} className="btn btn-success" onClick={this.finishQuiz}>Hoàn thành!</button> :
		  <button disabled={this.state.finalAnswers.length>0? false : true} className="btn btn-success" onClick={this.checkedAnswerBeforeMove}>Câu hỏi tiếp theo</button>
		  }
         </div>
		);
	}
	render(){
		return (
		    <div className="row">
		    	{this.renderQuestion()}
		    </div>
		);
	}
}
export default QuestionView;