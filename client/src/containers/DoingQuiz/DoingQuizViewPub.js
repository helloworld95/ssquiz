import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import _ from 'lodash';
import { Questions, Answers, Quizs } from '../../Collections/Collections';
import QuestionView from './QuestionView';
import removeValue from 'remove-value';
import ConfirmFinish from './ConfirmFinish'
import timeformat from 'format-duration';
import ResultTab from './ResultTab';

class DoingQuizViewPub extends Component {
  constructor(props) {
    super(props);
    this.state={
      currentQuestionPosition: 1,
      quizURL: this.props.params.quizURL,
      finalAnswers: [],
      sessionID: null,
      modal: false,
      isFinished: false,
      results: [],
      startedQuizAt: new Date().getTime(),
      timeFinished: null,
    };
    this.changeQuestion=this.changeQuestion.bind(this);
    this.renderQuestion=this.renderQuestion.bind(this);
    this.addUserAnswer=this.addUserAnswer.bind(this);
    this.removeUserAnswer=this.removeUserAnswer.bind(this);
    this.openConfirmModal=this.openConfirmModal.bind(this);
    this.closeConfirmModal=this.closeConfirmModal.bind(this);
    this.addQuestionResult = this.addQuestionResult.bind(this);
    this.showFinishedResult = this.showFinishedResult.bind(this);
    this.resetQuiz = this.resetQuiz.bind(this);
  }
  componentDidMount() {
    console.log(this.state)
    // this.props.router.setRouteLeaveHook(this.props.route, () => {
    //   if(!this.state.finish)
    //     return false;
    // })
  }
  componentWillUnmount() {
    this.setState({
      sessionID: '',
      isFinished: false
    });
    try {
      this.props.handler.stop();
    } catch (e) {
      console.log('the handler has not started yet! It is ok!');
    }
  }
  resetQuiz() {
    this.setState({
      currentQuestionPosition: 1,
      isFinished: false,
      results: [],
      startedQuizAt: new Date().getTime(),
      timeFinished: null,
    })
  }
  showFinishedResult() {
    var time = timeformat((new Date().getTime()) - this.state.startedQuizAt);
    this.setState({
      isFinished: true,
      timeFinished: time
    })
  }
  addQuestionResult(question, rightAnswers, triedTimes, time, cb) {
    var resultsArr = this.state.results;
    resultsArr.push({question, rightAnswers, triedTimes, time})
    this.setState({
        results: resultsArr
    }, () => {
      if(cb) cb();
    })
  }
  addUserAnswer(answerID) {
    let ansTemp = this.state.finalAnswers;
    ansTemp.push(answerID);
    this.setState({
      finalAnswers: ansTemp
    }, () => {
    })
  }
  removeUserAnswer(answerID) {
    let ansTemp = removeValue(this.state.finalAnswers, answerID);
    this.setState({
      finalAnswers: ansTemp
    }, () => {
    })
  }
  changeQuestion() {
    var nextQuestion = this.state.currentQuestionPosition + 1;
    this.setState({
      currentQuestionPosition: nextQuestion
    })
  }
  openConfirmModal() {
    this.setState({
      modal: true
    })
  }
  closeConfirmModal() {
    this.setState({
      modal: false
    })
  }

  renderQuestion() {
    if (!this.props.Questions) return (<li>Có lỗi xảy ra</li>);
    if (this.props.Questions.length <= 0) return (<li>Có lỗi xảy ra</li>);
    let queArr = this.props.Questions.map((question, i) => {
      i++;
      let answersArr = _.shuffle(this.props.Answers.find({question: question._id}).fetch());
      if(this.state.currentQuestionPosition == i)  {
        return (this.state.isFinished ? <ResultTab resetQuiz={this.resetQuiz} quizname={this.props.QuizTitle} results={this.state.results} time={this.state.timeFinished}/>:
        <QuestionView finishQuiz={this.showFinishedResult} addQuestionResult={this.addQuestionResult} isLast={i==this.props.Questions.length} changeNextQuestion={this.changeQuestion} checkedAnswers={this.state.finalAnswers} key={i} question={question} position={i} answers={_.shuffle(answersArr)} addUserAnswer={this.addUserAnswer} removeUserAnswer={this.removeUserAnswer}/>);
      } else {
        return '';
      }
    });
    return queArr;
  }
  render() {
    return (
      <div className="animated fadeIn">
      {/*<a onClick={this.openConfirmModal} style={{cursor: "pointer",float: "right", display: "inline-block"}} className="btn btn-success"><i className="fa fa-magic"></i>&nbsp; Hoàn thành</a>
    <ConfirmFinish open={this.state.modal} onToggle={this.closeConfirmModal} GameSession={this.state.sessionID} useranswers={this.state.finalAnswers}/>*/}
      {this.props.ready ? this.renderQuestion() : ''}
        
      </div>
    );
  }
}

export default createContainer((data) => {
  var handler = Meteor.subscribe('listQuestionsOfAQuiz', data.params.quizURL); 
  if (handler.ready()) {
    return {
      ready: true,
      handler: handler,
      Questions:  Questions.find({}, {sort: {createdAt: 1}}).fetch(),
      Answers: Answers,
      QuizTitle: Quizs.findOne({}, {fields: {title: 1}}).title
    };
  }
  return {ready: false};
}, DoingQuizViewPub);

