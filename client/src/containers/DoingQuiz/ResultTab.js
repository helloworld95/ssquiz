import React, { Component } from 'react';

class ResultTab extends Component {
    render() {
        var {quizname, results, time} = this.props;
        return (
            <div className="row">
                <div className="col-sm-6 col-md-4 animated bounceIn" style={{margin: "0 auto", float: "none", marginBottom: "10px"}}>
                    <div className="card" style={{width:"100%"}}>
                    <div className="card-header">
                        <strong style={{display: "inline"}}>
                        Kết quả bài quiz: {quizname}
                        </strong><br/>
                        &nbsp;
                        Tổng số câu hỏi: {results.length}
                        <br/>
                        &nbsp;
                        Tổng thời gian hoàn thành: {time}
                    </div>
                    <div className="card-block">
                        {results.map((result, index) => (
                            <div key={index} style={{display: "inline-block"}}>
                                <strong> Câu hỏi {index+1}: </strong> {result.question}. Đáp án: {result.rightAnswers.map(answer => answer)}. Số lần trả lời sai: {result.triedTimes}. Thời gian trả lời câu hỏi: {result.time} giây.
                            </div>
                        ))}
                    </div>
                </div>
                <button className="btn btn-success" onClick={this.props.resetQuiz}>Thử lại!</button>
            </div>
		    </div>
        );
    }
}

export default ResultTab;