import React, { Component } from 'react';

var Checkbox = React.createClass({
		getDefaultProps: function() {
   	 	return {
      	value: true,
        name: '',
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: '#c3c4c6',
        borderRadius: '4px',
        checkColor: '#60cd18',
        height: '1px',
        width: '',
        namePaddingLeft: '10px',
        namePaddingRight: ''
    	};
  	},
    render: function() {
        var style = {
        	boxStyle: {
          	borderWidth: this.props.borderWidth,
            borderStyle: this.props.borderStyle,
            borderColor: this.props.borderColor,
            borderRadius: this.props.borderRadius,
            paddingLeft: this.props.width,
  					paddingRight: this.props.width,
            paddingTop: this.props.height,
            paddingBottom: this.props.height
          },
          show: {
          	visibility: 'visible',
            color: this.props.checkColor,
            cursor: 'pointer'
          },
          hidden: {
          	visibility: 'hidden',
            color: this.props.checkColor
          },
          name: {
          	paddingLeft: this.props.namePaddingLeft,
            paddingRight: this.props.namePaddingRight,
            cursor: 'pointer',
            color: this.props.nameColor
          }
        };
        return (
        <div>
        	<span style={style.boxStyle}>
    				<i className="fa fa-check fa-lg" style={(this.props.value) ? style.show : style.hidden}></i>
					</span>
           <span style={style.name}>{this.props.name}</span>
        </div>
        );
    }
});

var WrapperCheckbox = React.createClass({ 
	getInitialState: function(){
        if(this.props.checked) {
            return {value: true}
        } else {
  	        return {value: false}
        }
    },
    handleClick: function(){
        this.setState({value: !this.state.value},
        () => {
            if (this.props.onChange) {
                this.props.onChange({target: {checked: this.state.value}})
            }
        });
    },
	render: function(){
  	return (
    	<div onClick={this.handleClick}>
    		<Checkbox name={this.props.name} value={this.state.value} nameColor={this.props.nameColor}/>
      </div>
    );
  }
});

export default WrapperCheckbox;