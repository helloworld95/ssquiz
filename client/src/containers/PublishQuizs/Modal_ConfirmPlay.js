import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import CreateGameActions from '../../Actions/CreateGameActions';
import {bindActionCreators} from 'redux';

class ModalPlayQuiz extends Component {
  constructor(props) {
    super(props);
    
    this.onConfirmHandler=this.onConfirmHandler.bind(this);
  }
  onConfirmHandler() {
    this.props.startQuiz(this.props.quiz.url);
  }
  render() {
    return (
      <Modal isOpen={this.props.open} toggle={this.props.onToggle} className={'modal-lg ' + this.props.className}>
        <ModalHeader toggle={this.toggle}>Bạn đã sẵn sàng?</ModalHeader>
        <ModalBody>
         <strong>Tiêu đề:</strong><p>{this.props.quiz.title}</p>
         <strong>Mô tả:</strong><p>{this.props.quiz.description}</p>
        </ModalBody>
        <ModalFooter>
        {this.props.data.loading ? <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div> :
          <div> 
            <Button color="primary" onClick={this.onConfirmHandler}>Đồng ý</Button>{' '}
            <Button color="secondary" onClick={this.props.onToggle}>Hủy</Button>
          </div>
        }
        </ModalFooter>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {data: state.gamesession};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({startQuiz: CreateGameActions.doCreateGame}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ModalPlayQuiz);