import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {Quizs} from '../../Collections/Collections';
import ModalPlayQuiz from './Modal_ConfirmPlay';

class PublishQuizCard extends Component {
	constructor(props) {
	    super(props);
	    this.state={
	    	modal: false,
	    	currentQuizSelected: ''
	    }
	    this.renderRows=this.renderRows.bind(this);
	    this.openConfirmModal=this.openConfirmModal.bind(this);
	    this.closeConfirmModal=this.closeConfirmModal.bind(this);
  	}
  	componentWillUnmount() {
	    try {
      		this.props.handler.stop();
	    } catch (e) {
	      	console.log('the handler has not started yet! It is ok!');
	    }
	  }
  	openConfirmModal(quiz) {
	    this.setState({
	      currentQuizSelected: quiz
	    }, () => {
	      this.setState({
	        modal: true
	      })
	    })
	}
	closeConfirmModal() {
	    this.setState({
	        modal: false
	    })
	}
  	renderRows() {
  		if (!this.props.quizs) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!"</td></tr>;
    	if (this.props.quizs.lenght<=0) return <tr><td>Hiện tại không có quizs nào trên hệ thống.</td></tr>;
    	let rows = this.props.quizs.map((quiz, index) => {
    		return (
    		<div key={index} className="col-md-12 animated bounceInUp">
	            <div className="card">
	              <div className="card-header">
	                <strong>
	                {quiz.title}
	                </strong>
	            	{/* <Link to={'publishedQuizs/'+quiz.url} style={{cursor: "pointer",float: "right", display: "inline-block"}} className="btn btn-outline-success btn-sm"><i className="fa fa-magic"></i>&nbsp; Tham gia</Link> */}
                	<a onClick={() => {this.openConfirmModal(quiz)}} style={{cursor: "pointer",float: "right", display: "inline-block"}} className="btn btn-outline-success btn-sm"><i className="fa fa-magic"></i>&nbsp; Tham gia</a>
	              </div>
	              <div className="card-block">
	              	<strong>
	                Mô tả: 
	                </strong>
	                <br/>&nbsp;{quiz.description}
	                 <br/>
	                <strong>
	                Tác giả: &nbsp;
	                </strong>
	                <br/>
	                <strong>
	                Thẻ: &nbsp;
	                </strong>
	              </div>
	            </div>
	        </div>
    		);
    	});
    	return rows;
  	}
	render() {
		return (
        <div className="row">
      		<ModalPlayQuiz open={this.state.modal} onToggle={this.closeConfirmModal} quiz={this.state.currentQuizSelected} />
        	{this.props.ready ? this.renderRows() : <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div>}
        </div>
		);
	}
}

export default createContainer((data) => {
  var handle = Meteor.subscribe('listPublishQuizs');
  if (handle.ready())
  return {
    ready: true,
    handler: handle,
    quizs: Quizs.find({$or: [{title: {$regex : ".*"+data.keyword+".*"}}, {description: {$regex : ".*"+data.keyword+".*"}}]}).fetch()
  };
  return {ready: false};
}, PublishQuizCard);
