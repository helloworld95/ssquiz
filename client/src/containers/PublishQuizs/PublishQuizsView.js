import React, { Component } from 'react';
import PublishQuizCard from './PublishQuizCard';

class PublishQuizsView extends Component {
  constructor(props) {
    super(props);
    this.state={
      searchkey: ''
    };
    this.onKeywordChange=this.onKeywordChange.bind(this);
  }
  onKeywordChange(event) {
    this.setState({
      searchkey: event.target.value
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
        <input type="text" name="text-input" className="form-control" onChange={this.onKeywordChange} placeholder="Tìm kiếm"/>
        <br/>
        <PublishQuizCard keyword={this.state.searchkey}/>
      </div>
    );
  }
}

export default PublishQuizsView;