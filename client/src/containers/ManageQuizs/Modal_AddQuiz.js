import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import CreateQuizActions from '../../Actions/CreateQuizActions';
import {bindActionCreators} from 'redux';

class ModalAddQuiz extends Component {
  constructor(props) {
    super(props);
    this.state={
      title: '',
      description: ''
    };
    this.onTitleChange=this.onTitleChange.bind(this);
    this.onDescriptionChange=this.onDescriptionChange.bind(this);
    this.onSubmitHandler=this.onSubmitHandler.bind(this);
  }
  onTitleChange(event) {
    this.setState({
      title: event.target.value
    })
  }
  onDescriptionChange(event) {
    this.setState({
      description: event.target.value
    })
  }
  onSubmitHandler() {
    let data = {title: this.state.title,description: this.state.description};
    this.props.submitData(data);
  }
  render() {
    return (
      <Modal isOpen={this.props.open} toggle={this.props.onToggle} className={'modal-lg ' + this.props.className}>
        <ModalHeader toggle={this.toggle}>Thêm Quiz mới</ModalHeader>
        <ModalBody>
          <div className="form-group row">
            <label className="col-md-3 form-control-label">Tiêu đề</label>
            <div className="col-md-9">
              <input type="text" name="text-input" className="form-control" onChange={this.onTitleChange} placeholder="Tiêu đề"/>
              <span className="help-block">Xin hãy điền tiêu đề</span>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-md-3 form-control-label">Mô tả</label>
            <div className="col-md-9">
              <textarea name="textarea-input" rows="9" className="form-control" onChange={this.onDescriptionChange} placeholder="Mô tả.."></textarea>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
        {this.props.data.loading ? <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div> :
          <div> 
            <Button color="primary" onClick={this.onSubmitHandler}>Thêm</Button>{' '}
            <Button color="secondary" onClick={this.props.onToggle}>Hủy</Button>
          </div>
        }
        </ModalFooter>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {data: state.quiz};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({submitData: CreateQuizActions.doCreateQuiz}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ModalAddQuiz);