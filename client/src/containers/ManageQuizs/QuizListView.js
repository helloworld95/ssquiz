import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import dateFormat from 'dateformat';
import InlineEdit from 'react-edit-inline';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import UpdateQuizActions from '../../Actions/UpdateQuizActions';
import {bindActionCreators} from 'redux';
import ConfirmModal from './Modal_ConfirmRemove';

import {Quizs} from '../../Collections/Collections';

class QuizListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      currentQuizSelected: ''
    }
    this.renderRows=this.renderRows.bind(this);
    this.onTitleChange=this.onTitleChange.bind(this);
    this.onDescriptionChange=this.onDescriptionChange.bind(this);
    this.onSelectQuiz=this.onSelectQuiz.bind(this);
    this.openConfirmModal=this.openConfirmModal.bind(this);
    this.closeConfirmModal=this.closeConfirmModal.bind(this);
  }
  componentWillUnmount() {
    try {
      this.props.handler.stop();
    } catch (e) {
      console.log('the handler has not started yet! It is ok!');
    }
  }
  onTitleChange(title) {
    this.props.submitTitleData(this.state.currentQuizSelected._id, title.text);
  }
  onDescriptionChange(description) {
    this.props.submitDesData(this.state.currentQuizSelected._id, description.text);
  }
  onSelectQuiz(quiz) {
    this.setState({
      currentQuizSelected: quiz
    })
  }
  openConfirmModal(quiz) {
    this.setState({
      currentQuizSelected: quiz
    }, () => {
      this.setState({
        modal: true
      })
    })
  }
  closeConfirmModal() {
    this.setState({
        modal: false
    })
  }
  renderRows() {
    let rows = [];
    if (!this.props.quizs) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!"</td></tr>;
    if (this.props.quizs.lenght<=0) return <tr><td>Bạn không có quiz nào, hãy tạo quiz bằng nút trên.</td></tr>;
    this.props.quizs.map((quiz, index) => {
      let date = new Date(quiz.createdAt);
      rows.push(
        <tr key={index} onClick={() => {this.onSelectQuiz(quiz)}}>
          <td><InlineEdit
              activeClassName="editing"
              text={quiz.title}
              paramName="text"
              change={this.onTitleChange}
              className="textshorten"
            />
          </td>
          <td><InlineEdit
              activeClassName="editing"
              text={quiz.description ? quiz.description : "Thêm mô tả..."}
              paramName="text"
              change={this.onDescriptionChange}
              className="textshorten"
            />
          </td>
          <td>{dateFormat(date, "d/mm/yyyy HH:MM:ss")}</td>
          <td>
            {quiz.status=='draft'?<span className="badge badge-default">Nháp</span>:""}
            {quiz.status=='pending'?<span className="badge badge-warning">Chờ duyệt</span>:""}
            {quiz.status=='denied'?<span className="badge badge-warning">Từ chối xuất bản</span>:""}
            {quiz.status=='published'?<span className="badge badge-success">Đã xuất bản</span>:""}
            {quiz.status=='remove'?<span className="badge badge-danger">Đã xóa</span>:""}
            {quiz.status=='cancelpending'?<span className="badge badge-warning">Yêu cầu gỡ</span>:""}
          </td>
          {quiz.status=='remove'? <td></td> : 
          <td>
            <Link to={'/myquizs/'+quiz.url}><i className="fa fa-edit fa-lg" style={{cursor: "pointer", display: "inline-block"}}></i></Link>&nbsp;
            <i className="fa fa-close fa-lg" style={{cursor: "pointer", display: "inline-block"}} onClick={() => {this.openConfirmModal(quiz)}}></i>
          </td>} 
          
        </tr>
      );
    });
    return rows;
  }
  render() {
    return (
      <div>
      <ConfirmModal open={this.state.modal} onToggle={this.closeConfirmModal} quiz={this.state.currentQuizSelected} />
        <table className="table">
          <thead>
            <tr>
              <th>Tiêu đề</th>
              <th>Mô tả</th>
              <th>Ngày tạo</th>
              <th>Trạng thái</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
            {this.props.ready ? this.renderRows() : <tr><td><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</td></tr>}
          </tbody>
        </table>
      </div>
    );
  }
}

var NewQuizListView  = createContainer((data) => {
  var handle = Meteor.subscribe('listOwnQuizs');
  if (handle.ready())
  return {
    ready: true,
    handler: handle,
    quizs: Quizs.find({$or: [{title: {$regex : ".*"+data.keyword+".*"}}, {description: {$regex : ".*"+data.keyword+".*"}}]}).fetch()
  };
  return {ready: false};
}, QuizListView);

const mapStateToProps = (state) => {
  return {data: state.quiz};
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    submitTitleData: UpdateQuizActions.doUpdateTitleQuiz, 
    submitDesData: UpdateQuizActions.doUpdateDesQuiz
  }, dispatch);
}


export default connect(mapStateToProps,mapDispatchToProps)(NewQuizListView);