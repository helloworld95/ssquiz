import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import RemoveQuizActions from '../../Actions/RemoveQuizActions';
import {bindActionCreators} from 'redux';

class ModalRemoveQuiz extends Component {
  constructor(props) {
    super(props);
    
    this.onRemoveHandler=this.onRemoveHandler.bind(this);
  }
  onRemoveHandler() {
    this.props.removeQuiz(this.props.quiz._id);
  }
  render() {
    return (
      <Modal isOpen={this.props.open} toggle={this.props.onToggle} className={'modal-lg ' + this.props.className}>
        <ModalHeader toggle={this.toggle}>Bạn có muốn xóa Quiz này?</ModalHeader>
        <ModalBody>
         <strong>Tiêu đề:</strong><p>{this.props.quiz.title}</p>
         <strong>Mô tả:</strong><p>{this.props.quiz.description}</p>
        </ModalBody>
        <ModalFooter>
        {this.props.data.loading ? <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div> :
          <div> 
            <Button color="danger" onClick={this.onRemoveHandler}>Đồng ý</Button>{' '}
            <Button color="secondary" onClick={this.props.onToggle}>Hủy</Button>
          </div>
        }
        </ModalFooter>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {data: state.removequiz};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({removeQuiz: RemoveQuizActions.doRemoveQuiz}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ModalRemoveQuiz);