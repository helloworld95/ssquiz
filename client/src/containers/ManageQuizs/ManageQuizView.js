import React, { Component } from 'react';
import QuizListView from './QuizListView';
import ModalAddQuiz from './Modal_AddQuiz';

class ManageQuizView extends Component {
  constructor(props) {
    super(props);
    this.state={
      modal: false,
      searchkey: ''
    };
    this.onKeywordChange=this.onKeywordChange.bind(this);
    this.toggleModal=this.toggleModal.bind(this);
  }
  toggleModal() {
    this.setState({
      modal: !this.state.modal
    })
  }
  onKeywordChange(event) {
    this.setState({
      searchkey: event.target.value
    })
  }
  render() {
    return (
      <div className="animated bounce">
        <div className="row">
          <div className="col-md-12">
          <button type="button" className="btn btn-primary" onClick={this.toggleModal}><i className="fa fa-plus"></i>&nbsp; Thêm Quiz</button>
          <ModalAddQuiz open={this.state.modal} onToggle={this.toggleModal}/>
            <div className="card">
              <div className="card-header">
                <i className="fa fa-align-justify"></i> Các Quizs bạn đang sở hữu
                <input type="text" name="text-input" className="form-control" onChange={this.onKeywordChange} placeholder="Tìm kiếm"/>
              </div>
              <div className="card-block">
                <QuizListView keyword={this.state.searchkey}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ManageQuizView;