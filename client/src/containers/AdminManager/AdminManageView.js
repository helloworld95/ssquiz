import React, {
	Component
} from 'react';
import {
	TabContent,
	TabPane,
	Nav,
	NavItem,
	NavLink
} from 'reactstrap';
import classnames from 'classnames';
import { createContainer } from 'meteor/react-meteor-data';
import PublishRequestsTable from './PublishRequestQuizs';
import PublishedTable from './PublishedQuizs';
import CancelRequestTable from './CancelRequestQuizs';
import AllUsersTable from './AllUsersList';



class AdminManageView extends Component {
	constructor(props) {
		super(props);
		this.toggle = this.toggle.bind(this);
		this.state = {
			activeTab: '1'
		};
	}

	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}
	render() {
		return (
			<div className="animated fadeIn">
		        <div className="row">
		          <div className="col-md-12 mb-2">
		            <Nav tabs>
		              <NavItem>
		                <NavLink style={{cursor: "pointer"}}
		                  className={classnames({ active: this.state.activeTab === '1' })}
		                  onClick={() => { this.toggle('1'); }}
		                >
		                  Các quizs chờ được xuất bản
		                </NavLink>
		              </NavItem>
		              <NavItem>
		                <NavLink style={{cursor: "pointer"}}
		                  className={classnames({ active: this.state.activeTab === '2' })}
		                  onClick={() => { this.toggle('2'); }}
		                >
		                  Các quizs đã được xuất bản
		                </NavLink>
		              </NavItem>
		              <NavItem>
		                <NavLink style={{cursor: "pointer"}}
		                  className={classnames({ active: this.state.activeTab === '3' })}
		                  onClick={() => { this.toggle('3'); }}
		                >
		                  Các quizs yêu cầu gỡ
		                </NavLink>
		              </NavItem>
		              <NavItem>
		                <NavLink style={{cursor: "pointer"}}
		                  className={classnames({ active: this.state.activeTab === '4' })}
		                  onClick={() => { this.toggle('4'); }}
		                >
		                  Danh sách thành viên
		                </NavLink>
		              </NavItem>
		            </Nav>
		            <TabContent activeTab={this.state.activeTab}>
		              <TabPane tabId="1">
		              	<PublishRequestsTable/>
		              </TabPane>
		              <TabPane tabId="2">
		              	<PublishedTable/>
		              </TabPane>
		              <TabPane tabId="3">
		              	<CancelRequestTable/>
		              </TabPane>
		              <TabPane tabId="4">
		              	<AllUsersTable/>
		              </TabPane>
		            </TabContent>
		          </div>
		        </div>
		    </div>
		);
	}
}

export default AdminManageView;
