import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Quizs } from '../../Collections/Collections';
import dateFormat from 'dateformat';
import { Link } from 'react-router';


class AllUsersTable extends Component {
	constructor(props) {
		super(props);
		this.renderRows=this.renderRows.bind(this);
	}
	componentWillUnmount() {
    try {
      this.props.handler.stop();
    } catch (e) {
      console.log('the handler has not started yet! It is ok!');
    }
	}
	renderRows() {
    let rows = [];
    if (!this.props.users) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!</td></tr>;
    if (this.props.users.lenght<=0) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!</td></tr>;
    this.props.users.map((user, index) => {
      let date = new Date(user.createdAt.toISOString());
      let emails = user.emails.map((email) => {
        return email.address ;
      });
      let roles = user.roles.calmandlearn_com.map((role) => {
        return role;
      });
      rows.push(
        <tr key={index}>
          <td>{user.profile.name}
          </td>
          <td>{emails[0]}
          </td>
          <td>{dateFormat(date, "d/mm/yyyy HH:MM:ss")}</td>
          <td>
            {roles.toString()}
          </td>
          <td>
            <Link to={'/'}><i className="fa fa-edit fa-lg" style={{cursor: "pointer", display: "inline-block"}}></i></Link>&nbsp;
            <i className="fa fa-close fa-lg" style={{cursor: "pointer", display: "inline-block"}}></i>
          </td>
          
        </tr>
      );
    });
    return rows;
  }
  render() {
    return (
        <table className="table">
          <thead>
            <tr>
              <th>Tên</th>
              <th>Email</th>
              <th>Ngày đăng ký</th>
              <th>Quyền</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
            {this.props.ready ? this.renderRows() : <tr><td><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</td></tr>}
          </tbody>
        </table>
    );
  }
}

export default createContainer((data) => {
  var handler = Meteor.subscribe('listUsers'); 
  if (handler.ready()) {
    return {
      ready: true,
      handler: handler,
      users: Meteor.users.find({}).fetch()
    };
  }
  return {ready: false};
}, AllUsersTable);