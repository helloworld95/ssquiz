import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Quizs } from '../../Collections/Collections';
import dateFormat from 'dateformat';
import { Link } from 'react-router';


class PublishRequestsTable extends Component {
	constructor(props) {
		super(props);
		this.renderRows=this.renderRows.bind(this);
	}
	componentWillUnmount() {
		try {
	      this.props.handler.stop();
	    } catch (e) {
	      console.log('the handler has not started yet! It is ok!');
	    }
	}
	renderRows() {
    let rows = [];
    if (!this.props.quizs) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!</td></tr>;
    if (this.props.quizs.lenght<=0) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!</td></tr>;
    this.props.quizs.map((quiz, index) => {
      let date = new Date(quiz.createdAt);
      rows.push(
        <tr key={index}>
          <td>{quiz.title}
          </td>
          <td>{quiz.description ? quiz.description : "Không có mô tả..."}
          </td>
          <td>{dateFormat(date, "d/mm/yyyy HH:MM:ss")}</td>
          <td>
            {quiz.status=='draft'?<span className="badge badge-default">Nháp</span>:""}
            {quiz.status=='pending'?<span className="badge badge-warning">Chờ duyệt</span>:""}
            {quiz.status=='published'?<span className="badge badge-success">Đã xuất bản</span>:""}
            {quiz.status=='remove'?<span className="badge badge-danger">Đã xóa</span>:""}
            {quiz.status=='cancelpending'?<span className="badge badge-warning">Yêu cầu gỡ</span>:""}
          </td>
          {quiz.status=='remove'? <td></td> : 
          <td>
            <Link to={'dashboard/quiz/'+quiz.url}><i className="fa fa-edit fa-lg" style={{cursor: "pointer", display: "inline-block"}}></i></Link>&nbsp;
            <i className="fa fa-close fa-lg" style={{cursor: "pointer", display: "inline-block"}}></i>
          </td>} 
          
        </tr>
      );
    });
    return rows;
  }
  render() {
    return (
        <table className="table">
          <thead>
            <tr>
              <th>Tiêu đề</th>
              <th>Mô tả</th>
              <th>Ngày tạo</th>
              <th>Trạng thái</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
            {this.props.ready ? this.renderRows() : <tr><td><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</td></tr>}
          </tbody>
        </table>
    );
  }
}

export default createContainer((data) => {
  var handler = Meteor.subscribe('listAllQuizs'); 
  if (handler.ready()) {
    return {
      ready: true,
      handler: handler,
      quizs: Quizs.find({status: "pending"}).fetch()
    };
  }
  return {ready: false};
}, PublishRequestsTable);