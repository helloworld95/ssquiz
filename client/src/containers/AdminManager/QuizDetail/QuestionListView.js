import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
// import AnswerRows from './AnswerRows';
import AdminPublishResolver from '../../../Actions/AdminPublishResolver';
import { Questions, Answers, Quizs } from '../../../Collections/Collections';


class QuestionListView extends Component {
  constructor(props) {
    super(props);
    this.renderQuestionCards=this.renderQuestionCards.bind(this);
    this.renderAnswerRows=this.renderAnswerRows.bind(this);
    this.renderButtons=this.renderButtons.bind(this);
  }
  componentWillUnmount() {
    try {
      this.props.handler.stop();
    } catch (e) {
      console.log('the handler has not started yet! It is ok!');
    }
  }
  renderAnswerRows(QuesID) {
    let answers = this.props.Answers.find({question: QuesID}).fetch();
    if (!answers) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!"</td></tr>;
      if (answers.lenght<=0) return <tr><td>Bạn không có quiz nào, hãy tạo câu trả lời bằng cách click vào "Thêm câu trả lời".</td></tr>;
    let rows = answers.map((answer, index) => {
      return (
      <div key={index} className="animated flash" style={{overflow:"auto", height: "30px"}}>
        {answer.answer}&nbsp;{answer.isRight?<i className="fa fa-check"></i>:''}
      </div>);
    });
    return rows;
  }
  renderQuestionCards() {
    if (!this.props.questions) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!"</td></tr>;
    if (this.props.questions.lenght<=0) return <tr><td>Bạn không có quiz nào, hãy tạo quiz bằng nút trên.</td></tr>;
    let rows = this.props.questions.map((question, index) => {
      let date = new Date(question.createdAt);
      return (
          <div className="col-sm-6 col-md-4 animated bounceIn" key={index}>
            <div className="card" style={{width:"100%"}}>
              <div className="card-header">
                <strong style={{display: "inline"}}>
                Câu hỏi số {index+1}:
                </strong><br/>
                {question.question}
                &nbsp;
              </div>
              <div className="card-block">
                <strong style={{display: "inline-block"}}>
                Các câu trả lời: &nbsp; 
                </strong>
                {this.renderAnswerRows(question._id)}
              </div>
            </div>
          </div>
      );
    });
    return rows;
  }
  renderButtons() {
    return (
      <div>
      {this.props.data.loading ? <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div> : 
      <div>
        {this.props.quiz.status == "pending" ? 
        <div>
          <button type="button" className="btn btn-success" onClick={() => {this.props.acceptPublish(this.props.quiz._id)}}>
            <i className="fa fa-check"></i>&nbsp; Chấp nhận yêu cầu
          </button> &nbsp;
          <button type="button" className="btn btn-danger" onClick={() => {this.props.denyPublish(this.props.quiz._id)}}>
            <i className="fa fa-close"></i>&nbsp; Huỷ yêu cầu
          </button>
        </div>:''}
        {this.props.quiz.status == "published" ? 
        <div>
          <button type="button" className="btn btn-danger">
            <i className="fa fa-close"></i>&nbsp; Gỡ quiz
          </button>
        </div>:''}
      </div>}
        
      </div>);
  }
  render() {
    return (
      <div>
      {this.props.ready ? this.renderButtons() : <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div>}
      <div className="row">
        {this.props.ready ? this.renderQuestionCards() : <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div>}
      </div>
      </div>
    );
  }
}


const newQuestionListView =  createContainer((data) => {
  var handler = Meteor.subscribe('quizDetailViewer',data.quizURL);
  if (handler.ready()) {
    return {
      ready: true,
      handler: handler,
      quiz: Quizs.findOne({url: data.quizURL}),
      questions: Questions.find({question: {$regex : ".*"+data.keyword+".*"}}).fetch(),
      Answers: Answers
    };
  }
  return {ready: false};
}, QuestionListView);

const mapStateToProps = (state) => {
  return {data: state.updatePublishRequest};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({acceptPublish: AdminPublishResolver.doAcceptRequestPublish, denyPublish: AdminPublishResolver.doDenyRequestPublish}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(newQuestionListView);