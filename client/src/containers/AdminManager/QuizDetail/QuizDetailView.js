import React, { Component } from 'react';
import QuestionListView from './QuestionListView';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';


class QuizDetailView extends Component {
  constructor(props) {
    super(props);
    this.state={
      searchkey: ''
    };
    this.onKeywordChange=this.onKeywordChange.bind(this);
  }
  onKeywordChange(event) {
    this.setState({
      searchkey: event.target.value
    })
  }
  render() {
    return (
      <div className="animated fadeIn">
        <input type="text" name="text-input" className="form-control" onChange={this.onKeywordChange} placeholder="Tìm kiếm"/>
        <QuestionListView keyword={this.state.searchkey} quizURL={this.props.params.quizURL}/>
      </div>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {data: state.requestpublish};
// }

// const mapDispatchToProps = (dispatch) => {
//   return bindActionCreators({
//     requestpublish: UpdateQuizActions.requestPublishQuiz
//   }, dispatch);
// }


// export default connect(mapStateToProps,mapDispatchToProps)(ManageQuestionView);
export default QuizDetailView;