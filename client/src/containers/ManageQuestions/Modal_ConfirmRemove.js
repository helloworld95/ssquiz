import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import RemoveQuestionActions from '../../Actions/RemoveQuestionActions';
import {bindActionCreators} from 'redux';

class ModalRemoveQuestion extends Component {
  constructor(props) {
    super(props);
    
    this.onRemoveHandler=this.onRemoveHandler.bind(this);
  }
  onRemoveHandler() {
    this.props.removeQuiz(this.props.question._id);
  }
  render() {
    return (
      <Modal isOpen={this.props.open} toggle={this.props.onToggle} className={'modal-lg ' + this.props.className}>
        <ModalHeader toggle={this.toggle}>Bạn có muốn xóa câu hỏi này? </ModalHeader>
        <ModalBody>
         <strong>Nội dung:</strong><p>{this.props.question.question}</p>
         <strong style={{color: "red"}}> Lưu ý: Các câu trả lời đi kèm sẽ bị xóa theo. </strong>
        </ModalBody>
        <ModalFooter>
        {this.props.data.loading ? <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div> :
          <div> 
            <Button color="danger" onClick={this.onRemoveHandler}>Đồng ý</Button>{' '}
            <Button color="secondary" onClick={this.props.onToggle}>Hủy</Button>
          </div>
        }
        </ModalFooter>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {data: state.removequestion};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({removeQuiz: RemoveQuestionActions.doRemoveQuestion}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ModalRemoveQuestion);