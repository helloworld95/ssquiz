import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import dateFormat from 'dateformat';
import InlineEdit from 'react-edit-inline';
import { connect } from 'react-redux';
import UpdateQuestionActions from '../../Actions/UpdateQuestionActions';
import CreateAnswerActions from '../../Actions/CreateAnswerActions';
import {bindActionCreators} from 'redux';
import ConfirmModal from './Modal_ConfirmRemove';
import AnswerRows from './AnswerRows';

import { Questions } from '../../Collections/Collections';

class QuestionListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmmodal: false,
      currentQuestionSelected: '',
      addAnswerText: 'Thêm câu trả lời...'
    }
    this.renderRows=this.renderRows.bind(this);
    this.onQuestionChange=this.onQuestionChange.bind(this);
    this.openConfirmModal=this.openConfirmModal.bind(this);
    this.closeConfirmModal=this.closeConfirmModal.bind(this);
    this.onSelectQuestion=this.onSelectQuestion.bind(this);
    this.onAddAnswer=this.onAddAnswer.bind(this);
  }
  componentWillUnmount() {
    try {
      this.props.QuestionsHandler.stop();
    } catch (e) {
      console.log('the handler has not started yet! It is ok!');
    }
  }
  onSelectQuestion(question) {
    this.setState({
      currentQuestionSelected: question
    })
  }
  onQuestionChange(question) {
    this.props.submitData(this.state.currentQuestionSelected._id, question.text);
  }
  onAddAnswer(answer) {
    this.props.addAnswer(this.state.currentQuestionSelected._id, answer.text, false);
    this.setState({
      addAnswerText: ''
    }, () => {
      this.setState({
        addAnswerText: 'Thêm câu trả lời...'
      })
    })
  }
  openConfirmModal(question) {
    this.setState({
      currentQuizSelected: question
    }, () => {
      this.setState({
        confirmmodal: true
      })
    })
  }
  closeConfirmModal() {
    this.setState({
        confirmmodal: false
    })
  }
  renderRows() {
    
    if (!this.props.questions) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!"</td></tr>;
    if (this.props.questions.lenght<=0) return <tr><td>Bạn không có quiz nào, hãy tạo quiz bằng nút trên.</td></tr>;
    let rows = this.props.questions.map((question, index) => {
      let date = new Date(question.createdAt);
      return (
          <div className="col-sm-6 col-md-4 animated bounceIn" key={index} onClick={() => {this.onSelectQuestion(question)}}>
            <div className="card" style={{width:"100%"}}>
              <div className="card-header">
                  <i className="fa fa-close fa-lg mt-2" style={{cursor: "pointer",float: "right", display: "inline"}} onClick={() => {this.openConfirmModal(question)}}></i>
                <strong style={{display: "inline"}}>
                Câu hỏi số {index+1}:
                </strong><br/>
                &nbsp;
                <InlineEdit
                    activeClassName="editing"
                    text={question.question}
                    paramName="text"
                    change={this.onQuestionChange}
                    className="textshorten-question"
                /> 
              </div>
              <div className="card-block">
                <strong style={{display: "inline-block"}}>
                Các câu trả lời: &nbsp; 
                </strong>
                  <AnswerRows questionID={question._id} quizURL={this.props.quizURL}/>
                <InlineEdit
                  activeClassName="editing"
                  text={this.state.addAnswerText}
                  paramName="text"
                  change={this.onAddAnswer}
                  className="textshorten-question"
                />
              </div>
            </div>
          </div>
      );
    });
    return rows;
  }
  render() {
    return (
      <div className="row">
      <ConfirmModal open={this.state.confirmmodal} onToggle={this.closeConfirmModal} question={this.state.currentQuestionSelected} />
        {this.props.ready ? this.renderRows() : <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div>}
      </div>
    );
  }
}

const NewQuestionListView = createContainer((data) => {
  var QuestionsHandler = Meteor.subscribe('listQuesBasedOnQuiz',data.quizURL);
  if (QuestionsHandler.ready()) {
    return {
      ready: true,
      QuestionsHandler: QuestionsHandler,
      questions: Questions.find({question: {$regex : ".*"+data.keyword+".*"}}, {sort: [ ["position", "asc"]] }).fetch()
    };
  }
  return {ready: false};
}, QuestionListView);

const mapStateToProps = (state) => {
  return {data: state.question};
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    submitData: UpdateQuestionActions.doUpdateQuestion,
    addAnswer: CreateAnswerActions.doCreateAnswer
  }, dispatch);
}


export default connect(mapStateToProps,mapDispatchToProps)(NewQuestionListView);