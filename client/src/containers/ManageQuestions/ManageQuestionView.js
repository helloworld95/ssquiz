import React, { Component } from 'react';
import QuestionListView from './QuestionListView';
import ModalAddQuestion from './Modal_AddQuestion';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import UpdateQuizActions from '../../Actions/UpdateQuizActions';


class ManageQuestionView extends Component {
  constructor(props) {
    super(props);
    this.state={
      modal: false,
      searchkey: ''
    };
    this.onKeywordChange=this.onKeywordChange.bind(this);
    this.toggleModal=this.toggleModal.bind(this);
    this.onRequestPublish=this.onRequestPublish.bind(this);
  }
  toggleModal() {
    this.setState({
      modal: !this.state.modal
    })
  }
  onKeywordChange(event) {
    this.setState({
      searchkey: event.target.value
    })
  }
  onRequestPublish(quizURL) {
    this.props.requestpublish(quizURL);
  }
  render() {
    return (
      <div className="animated fadeIn">
        <button type="button" className="btn btn-primary" onClick={this.toggleModal}><i className="fa fa-plus"></i>&nbsp; Thêm Câu hỏi</button>&nbsp;
        {this.props.data.loading ? <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div> : 
        <button type="button" className="btn btn-success" onClick={() => {this.onRequestPublish(this.props.params.quizURL)}}><i className="fa fa-rocket"></i>&nbsp; Xuất bản quiz</button>}
        <ModalAddQuestion open={this.state.modal} onToggle={this.toggleModal} quizURL={this.props.params.quizURL}/>
        <input type="text" name="text-input" className="form-control" onChange={this.onKeywordChange} placeholder="Tìm kiếm"/>
        <QuestionListView keyword={this.state.searchkey} quizURL={this.props.params.quizURL}/>
        <button type="button" className="btn btn-primary" onClick={this.toggleModal}><i className="fa fa-plus"></i>&nbsp; Thêm Câu hỏi</button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {data: state.requestpublish};
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    requestpublish: UpdateQuizActions.requestPublishQuiz
  }, dispatch);
}


export default connect(mapStateToProps,mapDispatchToProps)(ManageQuestionView);