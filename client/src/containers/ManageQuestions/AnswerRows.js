import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import InlineEdit from 'react-edit-inline';
import { connect } from 'react-redux';
import UpdateAnswerActions from '../../Actions/UpdateAnswerActions';
import RemoveAnswerActions from '../../Actions/RemoveAnswerActions';
import {bindActionCreators} from 'redux';

import {Answers} from '../../Collections/Collections';

class AnswerRows extends Component {
	constructor(props) {
		super(props);
		this.state={
			currentSelectAnswer: ''
		}
		this.onChangeSwitch=this.onChangeSwitch.bind(this);
		this.onAnswerChange=this.onAnswerChange.bind(this);
		this.onAnswerRemove=this.onAnswerRemove.bind(this);
		this.onSelectAnswer=this.onSelectAnswer.bind(this);
		this.renderRows=this.renderRows.bind(this);
	}
	componentWillUnmount() {
    	this.props.AnswersHandler.stop();
  	}
	onSelectAnswer(answer) {
		this.setState({
			currentSelectAnswer: answer
		})
	}
	onChangeSwitch(event, answer) {
	    this.props.updateAnswerisRight(answer._id, event.target.checked);
	 }
	onAnswerChange(answer) {
		this.props.updateAnswer(this.state.currentSelectAnswer._id, answer.text);
	}
	onAnswerRemove(answer) {
		this.props.removeAnswer(answer._id);
	}
	renderRows() {
		if (!this.props.answers) return <tr><td>Xin lỗi đã làm phiền bạn. Có lỗi xảy ra!"</td></tr>;
    	if (this.props.answers.lenght<=0) return <tr><td>Bạn không có quiz nào, hãy tạo câu trả lời bằng cách click vào "Thêm câu trả lời".</td></tr>;
		let rows = this.props.answers.map((answer, index) => {
			return (
			<div key={index} className="animated flash" style={{overflow:"auto", height: "30px"}} onClick={() => {this.onSelectAnswer(answer)}}>
	            <label className="switch switch-xs switch-3d switch-primary" style={{display: "inline-block"}}>
	              <input type="checkbox" className="switch-input" checked={answer.isRight} onChange={(event) => {this.onChangeSwitch(event, answer)}}/>
	              <span className="switch-label"></span>
	              <span className="switch-handle"></span>
	            </label>&nbsp;
	            <InlineEdit
	              activeClassName="editing"
	              text={answer.answer}
	              paramName="text"
	              change={this.onAnswerChange}
	              className="textshorten-question"
	            />
	            <i className="fa fa-close fa-lg" style={{cursor: "pointer",float: "right", display: "inline-block"}} onClick={() => {this.onAnswerRemove(answer)}}></i>
	        </div>);
		});
		return rows;
	}
	render() {

		return (
		<div>
             {this.props.ready ? this.renderRows() : <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div>}
         </div>
		);
	}
}

const NewAnswerRows = createContainer((data) => {
  var AnswersHandler = Meteor.subscribe('listQuesBasedOnQuiz',data.quizURL); 
  if (AnswersHandler.ready()) {
    return {
      ready: true,
      AnswersHandler: AnswersHandler,
      answers:  Answers.find({question: data.questionID}, {sort: [ ["position", "asc"]] }).fetch()
    };
  }
  return {ready: false};
}, AnswerRows);

const mapStateToProps = (state) => {
  return {data: state.question};
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    updateAnswer: UpdateAnswerActions.doUpdateAnswer,
    updateAnswerisRight: UpdateAnswerActions.doUpdateAnswerRight,
    removeAnswer: RemoveAnswerActions.doRemoveAnswer,
  }, dispatch);
}


export default connect(mapStateToProps,mapDispatchToProps)(NewAnswerRows);