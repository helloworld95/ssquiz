import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import CreateQuestionActions from '../../Actions/CreateQuestionActions';
import {bindActionCreators} from 'redux';

class ModalAddQuestion extends Component {
  constructor(props) {
    super(props);
    this.state={
      question: ''
    };
    this.onQuestionChange=this.onQuestionChange.bind(this);
    this.onSubmitHandler=this.onSubmitHandler.bind(this);
  }
  onQuestionChange(event) {
    this.setState({
      question: event.target.value
    })
  }
  onSubmitHandler() {
    this.props.submitData(this.props.quizURL, this.state.question);
  }
  render() {
    return (
      <Modal isOpen={this.props.open} toggle={this.props.onToggle} className={'modal-lg ' + this.props.className}>
        <ModalHeader toggle={this.toggle}>Thêm câu hỏi mới</ModalHeader>
        <ModalBody>
          <div className="form-group row">
            <label className="col-md-3 form-control-label">Câu hỏi:</label>
            <div className="col-md-9">
              <input type="text" name="text-input" className="form-control" onChange={this.onQuestionChange} placeholder="Câu hỏi"/>
              <span className="help-block">Xin hãy điền câu hỏi</span>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
        {this.props.data.loading ? <div><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</div> :
          <div> 
            <Button color="primary" onClick={this.onSubmitHandler}>Thêm</Button>{' '}
            <Button color="secondary" onClick={this.props.onToggle}>Hủy</Button>
          </div>
        }
        </ModalFooter>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {data: state.question};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({submitData: CreateQuestionActions.doCreateQuestion}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(ModalAddQuestion);