import {
	createNotification,
	NOTIFICATION_TYPE_SUCCESS,
	NOTIFICATION_TYPE_ERROR
} from 'react-redux-notify';
import React from 'react';

export const playGameSuccessNotification = {
	message: 'Quiz bắt đầu. Chúc bạn may mắn!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const playGameFailedNotification = {
	message: 'Có lỗi xảy ra! Xin mời bạn thử lại',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const finishQuizSuccessNotification = {
	message: 'Quiz kết thúc. Chúc mừng bạn đã hoàn thành quiz!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const finishQuizFailedNotification = {
	message: 'Có lỗi xảy ra! Xin mời bạn thử lại',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}