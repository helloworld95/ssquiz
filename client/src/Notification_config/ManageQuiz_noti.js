import {
	createNotification,
	NOTIFICATION_TYPE_SUCCESS,
	NOTIFICATION_TYPE_ERROR
} from 'react-redux-notify';
import React from 'react';

export const addQuizSuccessNotification = {
	message: 'Đã thêm quiz thành công!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const addQuizFailedNotification = {
	message: 'Tài khoản của bạn đã tồn tại quiz này! Xin hãy chọn tiêu đề khác.',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}


export const updateQuizSuccessNotification = {
	message: 'Cập nhật thành công!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const updateQuizFailedNotification = {
	message: 'Cập nhật thất bại!',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const removeQuizSuccessNotification = {
	message: 'Xóa thành công!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const removeQuizFailedNotification = {
	message: 'Xóa thất bại!',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}
