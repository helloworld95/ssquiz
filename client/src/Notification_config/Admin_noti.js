import {
	createNotification,
	NOTIFICATION_TYPE_SUCCESS,
	NOTIFICATION_TYPE_ERROR
} from 'react-redux-notify';
import React from 'react';

export const acceptSuccessNotification = {
	message: 'Xuất bản thành công!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const acceptFailedNotification = {
	message: 'Có lỗi xảy ra! Xin mời bạn thử lại',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const denySuccessNotification = {
	message: 'Từ chối xuất bản thành công!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const denyFailedNotification = {
	message: 'Có lỗi xảy ra! Xin mời bạn thử lại',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}