import {
	createNotification,
	NOTIFICATION_TYPE_SUCCESS,
	NOTIFICATION_TYPE_ERROR
} from 'react-redux-notify';
import React from 'react';

export const registerSuccessNotification = {
	message: 'Xin chúc mừng. Bạn đã đăng ký thành công!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const registerFailedNotification = {
	message: 'Tên tài khoản hoặc E-mail đã tồn tại. Xin thử lại!',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const loginSuccessNotification = {
	message: 'Chào mừng đến với ABC.',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const loginFailedNotification = {
	message: 'Tên tài khoản hoặc mật khẩu sai. Xin thử lại!',
	type: NOTIFICATION_TYPE_ERROR,
	icon: < i className = "fa fa-close" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}

export const logoutNotification = {
	message: 'Đăng xuất thành công!',
	type: NOTIFICATION_TYPE_SUCCESS,
	icon: < i className = "fa fa-check" / > ,
	duration: 5000,
	canDismiss: true,
	globalCustomNotification: false,
	forceClose: false
}