import React from 'react';
import { Router, Route, IndexRoute, browserHistory, IndexRedirect } from 'react-router';


// Containers
import Full from './containers/Full/'
import Simple from './containers/Simple/'
import ManageQuizView from './containers/ManageQuizs/ManageQuizView'
import ManageQuestionView from './containers/ManageQuestions/ManageQuestionView'
import DoingQuizView from './containers/DoingQuiz/DoingQuizView'
import DoingQuizViewPub from './containers/DoingQuiz/DoingQuizViewPub'
import PublishQuizsView from './containers/PublishQuizs/PublishQuizsView'
import AdminManageView from './containers/AdminManager/AdminManageView'
import QuizDetailView from './containers/AdminManager/QuizDetail/QuizDetailView'
import ManageHistoryView from './containers/ManageHistory/ManageHistoryView'
import HistoryDetailView from './containers/ManageHistory/HistoryDetailView'

import Login from './views/Pages/Login/'
import Register from './views/Pages/Register/'
import Page404 from './views/Pages/Page404/'
import Page500 from './views/Pages/Page500/'
import Dashboard from './views/Dashboard/'

const requireAuth = () => {
  if (!Meteor.userId()) {
    browserHistory.push({
      pathname: '/page/login'
    });
  }
};
const requireLogout = () => {
  if (Meteor.loggingIn() || Meteor.userId()) {
    browserHistory.push({
      pathname: '/'
    });
  }
};
const checkRole = () => {
  if (!Roles.userIsInRole(Meteor.userId(), ['admin'], 'calmandlearn.com')) {

    browserHistory.push({
      pathname: '/'
    });
  }
}

export default (
  <Router history={browserHistory}>
    <Route path="/" name="Trang chủ" component={Full} onEnter={ requireAuth }>
      <IndexRedirect to="/publishedQuizs" />
      <Route path="dashboard" name="Trang admin" onEnter={checkRole}>
        <IndexRoute component={AdminManageView}/>
        <Route path="quiz/:quizURL" component={QuizDetailView}/>
      </Route>
      <Route path="myquizs" name="Quản lý quizs">
        <IndexRoute component={ManageQuizView}/>
        <Route path=":quizURL" component={ManageQuestionView}/>
      </Route>
      <Route path="history" name="Lịch sử">
        <IndexRoute component={ManageHistoryView}/>
        <Route path=":gameID" component={HistoryDetailView}/>
      </Route>
      <Route path="publishedQuizs" name="Các quizs đã xuất bản">
        <IndexRoute component={PublishQuizsView}/>
        <Route path=":quizURL" component={DoingQuizViewPub}/>
      </Route>
    </Route>
    <Route path="/page" component={Simple} onEnter={ requireLogout }>
      <Route path="login" name="Trang đăng nhập" component={Login}/>
      <Route path="register" name="Trang đăng ký" component={Register}/>
      <Route path="500" name="Page 500" component={Page500}/>
    </Route>
    <Route path="doQuiz" name="Làm quiz">
        <Route path=":quizURL" component={DoingQuizViewPub}/>
    </Route>
    <Route path="*" component={ Page404 } />
  </Router>
);
