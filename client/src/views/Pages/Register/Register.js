import React, { Component } from 'react';
import { connect } from 'react-redux';
import RegisterActions from '../../../Actions/RegisterActions';
import {bindActionCreators} from 'redux';


class Register extends Component {
  constructor(props){
    super(props);
    this.state = {
      fullname: "",
      username: "",
      password: "",
      confirmpassword: "",
      email: "",
      passwordmatch: true
    };
    this.onUsernameChange=this.onUsernameChange.bind(this);
    this.onPasswordChange=this.onPasswordChange.bind(this);
    this.onEmailChange=this.onEmailChange.bind(this);
    this.handleSubmit=this.handleSubmit.bind(this);
    this.onConfirmPasswordChange=this.onConfirmPasswordChange.bind(this);
    this.onFullnameChange=this.onFullnameChange.bind(this);
  }
  onFullnameChange(event) {
     this.setState({fullname: event.target.value});
  }
  onUsernameChange(event) {
     this.setState({username: event.target.value});
  }
  onPasswordChange(event) {
     this.setState({password: event.target.value}, () => {
      if (this.state.password == this.state.confirmpassword) {
        this.setState({passwordmatch: true});
      } else {
        this.setState({passwordmatch: false});
      }
    });
  }
  onConfirmPasswordChange(event) {
    this.setState({confirmpassword: event.target.value}, () => {
      if (this.state.password == this.state.confirmpassword) {
        this.setState({passwordmatch: true});
      } else {
        this.setState({passwordmatch: false});
      }
    });
  }
  onEmailChange(event) {
     this.setState({email: event.target.value});
  }
  handleSubmit(event) {
    let state = this.state;
    if (state.fullname != '' && state.username != '' && state.email != '' && state.password != '' && state.confirmpassword != '' && state.passwordmatch)
    this.props.submitData(state.fullname, state.username, state.email, state.confirmpassword);
    event.preventDefault();
  }
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-6">
            <div className="card mx-2">
              <form className="card-block p-2" onSubmit={this.handleSubmit}>
                <h1>Đăng ký</h1>
                <p className="text-muted">Tạo tài khoản mới</p>

                <div className="input-group mb-1">
                  <span className="input-group-addon"><i className="icon-user"></i></span>
                  <input required={true} type="text" className="form-control" placeholder="Họ tên" onChange={this.onFullnameChange}/>
                </div>

                <div className="input-group mb-1">
                  <span className="input-group-addon"><i className="icon-user"></i></span>
                  <input required={true} type="text" className="form-control" placeholder="Tên đăng nhập" onChange={this.onUsernameChange}/>
                </div>

                <div className="input-group mb-1">
                  <span className="input-group-addon">@</span>
                  <input required={true} type="email" className="form-control" placeholder="Email" onChange={this.onEmailChange}/>
                </div>

                <div className="input-group mb-1">
                  <span className="input-group-addon"><i className="icon-lock"></i></span>
                  <input required={true} type="password" className="form-control" placeholder="Mật khẩu" onChange={this.onPasswordChange}/>
                </div>

                <div className={this.state.passwordmatch ? "input-group mb-2" : "input-group mb-2 has-danger"}>
                  <span className="input-group-addon"><i className="icon-lock"></i></span>
                  <input required={true} type="password" className={this.state.passwordmatch ? "form-control" : "form-control form-control-danger"} placeholder="Xác nhận mật khẩu" onChange={this.onConfirmPasswordChange}/>
                </div>
                {this.props.data.loading ? 
                  <a className="btn btn-block btn-success"><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</a>:
                <input type="submit" className="btn btn-block btn-success" value="Đăng ký"/>}
                
              </form>
              <div className="card-footer p-2">
                <div className="row">
                  <div className="col-6">
                    <button className="btn btn-block btn-facebook" type="button"><span>facebook</span></button>
                  </div>
                  <div className="col-6">
                    <button className="btn btn-block btn-twitter" type="button"><span>twitter</span></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {data: state.register};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({submitData: RegisterActions.doRegister, redirect: RegisterActions.redirectOnSuccesRegister}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Register);
