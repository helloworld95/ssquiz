import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import LoginActions from '../../../Actions/LoginActions';
import {bindActionCreators} from 'redux';

class Login extends Component {
   constructor(props){
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    this.onUsernameChange=this.onUsernameChange.bind(this);
    this.onPasswordChange=this.onPasswordChange.bind(this);
    this.handleSubmit=this.handleSubmit.bind(this);
  }
  onUsernameChange(event) {
     this.setState({username: event.target.value});
  }
  onPasswordChange(event) {
     this.setState({password: event.target.value});
  }
  handleSubmit(event) {
    let state = this.state;
    this.props.submitData(state.username, state.password);
    event.preventDefault();
  }
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className="card-group mb-0">
              <form className="card p-2" onSubmit={this.handleSubmit}>
                <div className="card-block">
                  <h1>Đăng nhập</h1>
                  <p className="text-muted">Đăng nhập tài khoản</p>
                  <div className="input-group mb-1">
                    <span className="input-group-addon"><i className="icon-user"></i></span>
                    <input required={true} type="text" className="form-control" placeholder="Tài khoản" onChange={this.onUsernameChange}/>
                  </div>
                  <div className="input-group mb-2">
                    <span className="input-group-addon"><i className="icon-lock"></i></span>
                    <input required={true} type="password" className="form-control" placeholder="Mật khẩu" onChange={this.onPasswordChange}/>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      {this.props.data.loading ? 
                        <a className="btn btn-block btn-success"><i className="fa fa-spinner fa-spin"></i>Vui lòng chờ...</a>:
                        <input type="submit" className="btn btn-block btn-success" value="Đăng nhập"/>}
                    </div>
                    <div className="col-6 text-right">
                      <button type="button" className="btn btn-link px-0">Forgot password?</button>
                    </div>
                  </div>
                </div>
              </form>
              <div className="card card-inverse card-primary py-3 hidden-md-down" style={{ width: 44 + '%' }}>
                <div className="card-block text-center">
                  <div>
                    <h2>Đăng ký ngay!</h2>
                    <p>SoSub Quiz is the best!</p>
                    <Link to={'/page/register'} className="btn btn-primary active mt-1">Đăng ký!</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {data: state.login};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({submitData: LoginActions.doLogin}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);