import RegisterActions from '../Actions/RegisterActions';
import {
	eventChannel,
	END
} from 'redux-saga';

const createGameSession = (quizURL) => {
	return eventChannel(emitter => {
		Meteor.call('GameSessions.create', quizURL, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}

const finishQuiz = (GameSessionsID, answerIDs) => {
	return eventChannel(emitter => {
		Meteor.call('GameSessions.finish', GameSessionsID, answerIDs, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
export default {
	createGameSession,
	finishQuiz
}