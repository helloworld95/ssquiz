import RegisterActions from '../Actions/RegisterActions';
import {
	eventChannel,
	END
} from 'redux-saga';

const registerUser = (fullname, username, email, password) => {
	return eventChannel(emitter => {
		Meteor.call('account.create', fullname, username, email, password, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}

const loginUser = (username, password) => {
	return eventChannel(emitter => {
		if (Meteor.userId()) {
			Meteor.logout(function(err) {
				if (err) {
					console.log(err);
				} else {
					Meteor.loginWithPassword(username, password, function(error) {
						if (error) {
							emitter(false);
						} else {
							emitter(Meteor.userId());
						}
					});
				}
			});
		} else {
			Meteor.loginWithPassword(username, password, function(error) {
				if (error) {
					emitter(false);
				} else {
					emitter(Meteor.userId());
				}
			});
		}

		return () => {
			console.log('stopped');
		}
	});

}

const logoutUser = () => {
	return eventChannel(emitter => {
		if (Meteor.userId()) {
			Meteor.logout(function(err) {
				emitter(true);
			});
		} else {
			emitter(true);
		}

		return () => {
			console.log('stopped');
		}
	});

}

export default {
	registerUser,
	loginUser,
	logoutUser
}