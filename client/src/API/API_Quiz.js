import RegisterActions from '../Actions/RegisterActions';
import {
	eventChannel,
	END
} from 'redux-saga';

const createNewQuiz = (quiz) => {
	return eventChannel(emitter => {
		Meteor.call('quiz.create', quiz, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}

const updateTitleQuiz = (quiz, title) => {
	return eventChannel(emitter => {
		Meteor.call('quiz.update.title', quiz, title, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}

const updateDescriptionQuiz = (quiz, description) => {
	return eventChannel(emitter => {
		Meteor.call('quiz.update.description', quiz, description, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}

const requestPublishQuiz = (quizURL) => {
	return eventChannel(emitter => {
		Meteor.call('quiz.requestPublish', quizURL, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}

const removeQuiz = (quiz) => {
	return eventChannel(emitter => {
		Meteor.call('quiz.remove', quiz, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
export default {
	createNewQuiz,
	updateTitleQuiz,
	updateDescriptionQuiz,
	removeQuiz,
	requestPublishQuiz
}