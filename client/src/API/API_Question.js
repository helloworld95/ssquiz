import RegisterActions from '../Actions/RegisterActions';
import {
	eventChannel,
	END
} from 'redux-saga';

const createNewQuestion = (quizURL, question) => {
	return eventChannel(emitter => {
		Meteor.call('question.create', quizURL, question, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
const updateQuestion = (questionID, question) => {
	return eventChannel(emitter => {
		Meteor.call('question.update', questionID, question, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}

const removeQuestion = (questionID) => {
	return eventChannel(emitter => {
		Meteor.call('question.remove', questionID, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
export default {
	createNewQuestion,
	updateQuestion,
	removeQuestion
}