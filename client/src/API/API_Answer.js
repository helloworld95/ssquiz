import RegisterActions from '../Actions/RegisterActions';
import {
	eventChannel,
	END
} from 'redux-saga';

const createNewAnswer = (questionID, answer, isRight) => {
	return eventChannel(emitter => {
		Meteor.call('answer.create', questionID, answer, isRight, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
const updateAnswer = (answerID, answer) => {
	return eventChannel(emitter => {
		Meteor.call('answer.update', answerID, answer, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
const updateAnswerisRight = (answerID, isRight) => {
	return eventChannel(emitter => {
		Meteor.call('answer.update.isRight', answerID, isRight, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
const removeAnswer = (answerID) => {
	return eventChannel(emitter => {
		Meteor.call('answer.remove', answerID, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
export default {
	createNewAnswer,
	updateAnswer,
	removeAnswer,
	updateAnswerisRight
}