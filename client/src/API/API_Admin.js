import {
	eventChannel,
	END
} from 'redux-saga';

const acceptPublishRequest = (quizID) => {
	return eventChannel(emitter => {
		Meteor.call('admin.publishQuiz', quizID, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
const denyPublishRequest = (quizID) => {
	return eventChannel(emitter => {
		Meteor.call('admin.denyQuiz', quizID, function(error, result) {
			emitter(result);
		});
		return () => {
			console.log('stopped');
		}
	});

}
export default {
	acceptPublishRequest,
	denyPublishRequest
}