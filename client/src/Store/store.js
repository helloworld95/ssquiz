import logger from 'redux-logger';
import reducer from '../Reducers/index.js';
import promise from "redux-promise-middleware";
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import sagas from '../Sagas/saga.js';

const sagaMiddleware = createSagaMiddleware();
const store = applyMiddleware(promise(), logger(), sagaMiddleware)(createStore)(reducer);
sagaMiddleware.run(sagas);
export default store;