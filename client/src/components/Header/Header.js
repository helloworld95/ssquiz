import React, { Component } from 'react';
import { Dropdown, DropdownMenu, DropdownItem } from 'reactstrap';
import { connect } from 'react-redux';
import LogoutActions from '../../Actions/LogoutActions';
import {bindActionCreators} from 'redux';

class Header extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
    this.logoutUser=this.logoutUser.bind(this);
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  sidebarToggle() {
    document.body.classList.toggle('sidebar-hidden');
  }

  mobileSidebarToggle() {
    document.body.classList.toggle('sidebar-mobile-show');
  }

  logoutUser() {
    this.props.logout();
  }
  render() {
    return (
      <header className="app-header navbar">
        <button className="navbar-toggler mobile-sidebar-toggler hidden-lg-up" onClick={this.mobileSidebarToggle} type="button">&#9776;</button>
        <a className="navbar-brand" href="#"></a>
        <ul className="nav navbar-nav hidden-md-down">
          <li className="nav-item">
            <a className="nav-link navbar-toggler sidebar-toggler" onClick={this.sidebarToggle} href="#">&#9776;</a>
          </li>
        </ul>
        <ul className="nav navbar-nav ml-auto">
          <li className="nav-item">
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <a onClick={this.toggle} className="nav-link dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded={this.state.dropdownOpen}>
                <img src='https://quizapp.calmandlearn.com/img/avatars/avatar.png' className="img-avatar" alt="admin@bootstrapmaster.com"/>
                <span className="hidden-md-down">admin</span>
              </a>
              <DropdownMenu className="dropdown-menu-right">
                <DropdownItem header className="text-center"><strong>Settings</strong></DropdownItem>
                <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
                <DropdownItem divider />
                <div onClick={this.logoutUser}><DropdownItem><i className="fa fa-lock"></i> Logout</DropdownItem></div>
              </DropdownMenu>
            </Dropdown>
          </li>
          <li className="nav-item">
          </li>
        </ul>
      </header>
    )
  }
}
const mapStateToProps = (state) => {
  return {data: state.login};
}
const mapDispatchToProps = (dispatch) => {
   return bindActionCreators({logout: LogoutActions.doLogout}, dispatch);
}


export default connect(mapStateToProps,mapDispatchToProps)(Header);