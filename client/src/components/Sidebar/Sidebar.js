import React, { Component } from 'react';
import { Link } from 'react-router'

class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
  }

  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }

  render() {
    return (

      <div className="sidebar">
        <nav className="sidebar-nav">
          <ul className="nav">
          {Roles.userIsInRole(Meteor.userId(), ['admin'], 'calmandlearn.com')? 
          <li className="nav-item">
              <Link to={'/dashboard'} className="nav-link" activeClassName="active"><i className="icon-speedometer"></i> Dashboard <span className="badge badge-info">ADMIN</span></Link>
          </li> :""
          }
          <li className="nav-item">
            <Link to={'/publishedQuizs'} className="nav-link" activeClassName="active"><i className="fa fa-bars fa-lg mt-2"></i> Quizs</Link>
          </li>
          <li className="nav-item">
            <Link to={'/history'} className="nav-link" activeClassName="active"><i className="fa fa-history fa-lg mt-2"></i> Lịch sử</Link>
          </li>
          <li className="nav-item">
            <Link to={'/myquizs'} className="nav-link" activeClassName="active"><i className="fa fa-pencil-square-o fa-lg mt-2"></i> Quizs của bạn</Link>
          </li>
          </ul>
        </nav>
      </div>
    )
  }
}

export default Sidebar;
