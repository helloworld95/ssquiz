import ActionTypes from '../Actions/ActionTypes';

export default function(state = {
	loading: false, 
	success: null
}, action) {
	switch (action.type) {
		case ActionTypes.REMOVE_QUIZ:
			return {
				loading: true,
				success: null
			};
		case ActionTypes.REMOVE_QUIZ_SUCCESS:
			return {
				loading: false,
				success: true
			};
		case ActionTypes.REMOVE_QUIZ_FAILED:
			return {
				loading: false,
				success: false
			};
	}
	return state;
}