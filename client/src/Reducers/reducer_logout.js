import ActionTypes from '../Actions/ActionTypes';

export default function(state = {
	loading: false
}, action) {
	switch (action.type) {
		case ActionTypes.LOGOUT:
			return {
				loading: true
			};
		case ActionTypes.DONE_LOGOUT:
			return {
				loading: false
			};
	}
	return state;
}