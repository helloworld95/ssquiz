import ActionTypes from '../Actions/ActionTypes';

export default function(state = {
	loading: false, 
	success: null
}, action) {
	switch (action.type) {
		case ActionTypes.LOGIN:
			return {
				loading: true,
				success: null
			};
		case ActionTypes.LOGIN_SUCCESS:
			return {
				loading: false,
				success: true
			};
		case ActionTypes.LOGIN_FAILED:
			return {
				loading: false,
				success: false
			};
	}
	return state;
}