import ActionTypes from '../Actions/ActionTypes';

export default function(state = {
	loading: false, 
	success: null
}, action) {
	switch (action.type) {
		case ActionTypes.REGISTER:
			return {
				loading: true,
				success: null
			};
		case ActionTypes.REGISTER_SUCCESS:
			return {
				loading: false,
				success: true
			};
		case ActionTypes.REGISTER_FAILED:
			return {
				loading: false,
				success: false
			};
		case ActionTypes.REGISTER_SUCCESS_REDIRECT:
			return {
				loading: false,
				success: true
			};
	}
	return state;
}