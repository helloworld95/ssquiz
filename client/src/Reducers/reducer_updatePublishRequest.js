import ActionTypes from '../Actions/ActionTypes';

export default function(state = {
	loading: false, 
	success: null
}, action) {
	switch (action.type) {
		case ActionTypes.ACCEPT_REQUEST_PUBLISH:
			return {
				loading: true,
				success: null
			};
		case ActionTypes.ACCEPT_REQUEST_PUBLISH_SUCCESS:
			return {
				loading: false,
				success: true
			};
		case ActionTypes.ACCEPT_REQUEST_PUBLISH_FAILED:
			return {
				loading: false,
				success: false
			};
		case ActionTypes.DENY_REQUEST_PUBLISH:
			return {
				loading: true,
				success: null
			};
		case ActionTypes.DENY_REQUEST_PUBLISH_SUCCESS:
			return {
				loading: false,
				success: true
			};
		case ActionTypes.DENY_REQUEST_PUBLISH_FAILED:
			return {
				loading: false,
				success: false
			};
	}
	return state;
}