import { combineReducers } from 'redux'; 
import RegisterReducer from './reducer_register';
import LoginReducer from './reducer_login';
import notifyReducer from 'react-redux-notify';
import CreateQuizReducer from './reducer_createquiz';
import CreateQuestionReducer from './reducer_createquestion';
import RemoveQuizReducer from './reducer_removequiz';
import RemoveQuestionReducer from './reducer_removequestion';
import CreateGameSessionReducer from './reducer_creategame';
import FinishQuizReducer from './reducer_finishquiz';
import RequestPublishQuizReducer from './reducer_requestpublish';
import UpdatePublishRequest from './reducer_updatePublishRequest';

const rootReducer = combineReducers({
	register: RegisterReducer,
	login: LoginReducer,
	notifications: notifyReducer,
	quiz: CreateQuizReducer,
	removequiz: RemoveQuizReducer,
	question: CreateQuestionReducer,
	removequestion: RemoveQuestionReducer,
	gamesession: CreateGameSessionReducer,
	finishquiz: FinishQuizReducer,
	requestpublish: RequestPublishQuizReducer,
	updatePublishRequest: UpdatePublishRequest
});

export default rootReducer;