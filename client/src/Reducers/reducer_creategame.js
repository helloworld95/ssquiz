import ActionTypes from '../Actions/ActionTypes';

export default function(state = {
	loading: false, 
	success: null
}, action) {
	switch (action.type) {
		case ActionTypes.CREATE_GAME_SESSION:
			return {
				loading: true,
				success: null
			};
		case ActionTypes.CREATE_GAME_SESSION_SUCCESS:
			return {
				loading: false,
				success: true,
				payload: action.GameSessionsID
			};
		case ActionTypes.CREATE_GAME_SESSION_FAILED:
			return {
				loading: false,
				success: false
			};
	}
	return state;
}