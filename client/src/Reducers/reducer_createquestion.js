import ActionTypes from '../Actions/ActionTypes';

export default function(state = {
	loading: false, 
	success: null
}, action) {
	switch (action.type) {
		case ActionTypes.CREATE_QUESTION:
			return {
				loading: true,
				success: null
			};
		case ActionTypes.CREATE_QUESTION_SUCCESS:
			return {
				loading: false,
				success: true
			};
		case ActionTypes.CREATE_QUESTION_FAILED:
			return {
				loading: false,
				success: false
			};
	}
	return state;
}